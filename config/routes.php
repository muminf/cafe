<?php
	return array(
         
            "shop/report/PrintCash/(\d+)" => "report/PrintCash/$1",
            "shop/report/Cash/(\d{2}-\d{2}-\d{4})/(\d{2}-\d{2}-\d{4})" => "report/Cash/$1/$2", //Дописать регулярку для фильтра
            "shop/report/Filter" => "report/Filter",
            
            "shop/report/Payments" => "report/Payments",
            "shop/report/Payments/(\d{2}-\d{2}-\d{4})/(\d{2}-\d{2}-\d{4})/(\d+)/(\d+)" => "report/Payments/$1/$2/$3/$4",
            
            "shop/user/login" => "user/login",
            "shop/user/logout" => "user/logout",
            "shop/user/ChangePassword" => "user/ChangePassword",
            
          
            "shop/sklad/list/(\d{2})-(\d{2})-(\d{4})/(\d{2})-(\d{2})-(\d{4})" => "sklad/list/$1/$2", //Дописать регулярку для фильтра
        
            
            
            "shop/kassa/getByShCode/(\d+)" => "kassa/getByShCode/$1",
            "shop/kassa/getByName/(\S)" => "kassa/getByName/$1",
            "shop/kassa/DelFromCart" => "kassa/DelFromCart",
            "shop/kassa/CloseSale" => "kassa/CloseSale",
            "shop/kassa/AddRashod" => "kassa/AddRashod",
            
            "shop/kassa/arr" => "kassa/arr",
            "shop/kassa" => "kassa/index",
            
          
            
            "shop" => "site/index"						  // actionIndex в SiteController
            
				 );