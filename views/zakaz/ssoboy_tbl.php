<title><?php echo $info["client"]?></title>
<?php include ROOT . '/views/layouts/header.php'; 

if(!isset($_SESSION["zakazListSsoboy"])){
    $_SESSION["zakazListSsoboy"] = array();
}
$zakazAll = (isset($_SESSION["zakazListSsoboy"][$id]) && count($_SESSION["zakazListSsoboy"][$id])>0 ? "block" : "none");

?>

<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-1">
            <a href="/shop/zakaz/">
                <button type='button' class='btn btn-block btn-warning'>Столы</button>
            </a>
        </div>
        <div class="col-sm-1">
            <a href="/shop/zakaz/Ssoboy/<?php echo $info["id"] ?>">
                <button type='button' class='btn btn-block btn-warning'>Категории</button>
            </a>
        </div>
        <div class="col-sm-4">
            <h1><?php echo $info["client"]?></h1>
        </div>
        <div class="col-sm-4">
            <h1>Официант: <?php echo $info["oficiant"]?></h1>
        </div>
            
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row"> 
            <div class="col-md-12">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Список заказов</h3>
               <h3 style="display:block;float:right;" class="card-title">Всего на сумму: <div id="allSum" style="display:inline"><?php echo $prihodSum; ?></div></h3>
             </div>
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="zakazListTable">
                 <tr>
                   <th>Наименование</th>
                   <th>Количество</th>
                   <?php if($_SESSION["user"]["type"] == User::admin){?>
                       <th>Отменить</th>
                   <?php } ?>
                   <th>Цена</th>
                   <th>Сумма</th>
                   <th>Статус</th>
                   <th style="width:10px"></th>
                 </tr>
                 <?php #unset($_SESSION["prihodList"]);   
                    if(count($list)>0){
                    foreach($list as $k=>$v){
                        $v["status"] = ($v["id_status"] == Zakaz::state_ready ? "<a href='/shop/zakaz/ServeToClient/".$id."/".$v["id"]."' class='ServeToClient' id=".$v["id"]."><button type='button' class='btn btn-block btn-warning btn-sm'>Подать</button></a>"  : $v["status"]);
                        $delButton = '<a class="prihodItem" id="'.$v["id"].'" href="/shop/sklad/DelFromPrihodList/'.$v["id"].'"><i class="nav-icon fa fa-remove"></i></a>';
                        $delButton = ($v["id_status"] = Zakaz::state_podan ? "" : $delButton);
                ?>
                 <tr>
                   <td><?php echo $v["product"] ?></td>
                   <td><?php echo $v["count"] ?></td>
                   <?php if($_SESSION["user"]["type"] == User::admin){?>
                   <td>
                       <form action="/shop/zakaz/CancelZakaz/<?php echo $v["id"]?>/<?php echo $v["id_product"] ?>/<?php echo $info["id"] ?>" method="POST"><input style="margin-top: -5px" type="submit" class="btn btn-sm btn-warning" name="return" value="Возврат"><input style="width:60px" class="cancelCount" name="count" value="<?php echo $v["count"]?>" type="number"><input style="margin-top: -5px" type="submit" class="btn btn-sm btn-warning" name="remove" value="Списать"></form>
                   </td>
                        
                       <?php } ?>
                   <td><?php echo $v["price_out"] ?></td>
                   <td><?php echo $v["count"]*$v["price_out"] ?></td>
                   <td><?php echo $v["status"] ?></td>
                   <td><?php echo $delButton ?></td>
                 </tr>
                    <?php }} ?>
                 
                <?php 
                    if(isset($_SESSION["zakazListSsoboy"][$id])){
                    foreach($_SESSION["zakazListSsoboy"][$id] as $k=>$v){ 
                ?>
                 <tr id="<?php echo $k?>" class="list">
                   <td><?php echo $v["name"] ?></td>
                  <!-- <td><input type='number' class="count" name='count<?php echo$v["id"] ?>' value='<?php echo $v["count"] ?>' style="width:65px;display:inline"><a href="/shop/zakaz/Cancel/<?php echo $info["id"]?>/<?php echo $k?>" class="btn-sm btn-block btn-warning" style="width:65px;display:inline">Отмена</></td> -->
                    <td>
                        <?php if($v["count"]>0){?>
                        <a id="<?php echo $k?>" href="/shop/zakaz/MinusFromZakazListSsoboy/<?php echo $info["id"]?>/<?php echo $k?>" class="btn-sm btn-block btn-warning minusFromZakazList" style="width:65px;display:inline">-</a>
                        <?php }?>
                            <span id="<?php echo $k?>" class="count"><?php echo $v["count"] ?></span>
                        <?php if($v["count"]>0){?>
                        <a id="<?php echo $k?>" href="/shop/zakaz/AddToZakazListAjaxSsoboy/<?php echo $info["id"]?>/<?php echo $k?>" class="btn-sm btn-block btn-warning plusToZakazList" style="width:65px;display:inline">+</a>
                        <?php }?>
                    </td>
                    
                   <td><?php echo $v["price_out"] ?></td>
                   <td><?php echo $v["count"]*$v["price_out"] ?></td>
                   <td>Новый
                      <!-- <a href="/shop/zakaz/SendFromZakazList/<?php echo $id?>/<?php echo $k?>" class="SendFromZakazList" id="<?php echo $k ?>">
                           <button type='button' class='btn btn-block btn-warning btn-sm'>Заказать</button>
                       </a>  -->
                   </td>
                   <td></td>
                 </tr>
                <?php }} ?>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->
            </div>
            <div class="col-md-4">
                <a href="/shop/zakaz/SendZakazListSsoboy/<?php echo $id ?>">
                    <button style="display:<?php echo $zakazAll ?>" type='button' class='btn btn-block btn-warning btn-block' id="zakazAll">Заказать всё</button>
                </a>
            </div>
          <br>
    <table class="table table-hover" id="zakazMenuList">
        <thead>
          <tr>
          <th>Наименование</th>
          <th>Цена</th>
          <th>Остаток</th>
          <th>Категория</th>
        </tr>
        </thead>
        <tbody>      
<?php foreach($menu as $k=>$v){ ?>     
        <tr class="addToZakazListSsoboy" stol='<?php echo $info["id"] ?>' href="/shop/zakaz/AddToZakazListAjaxSsoboy/<?php echo $info["id"]?>/<?php echo $k?>" id="<?php echo $k ?>">
            <td>
                <?php echo $v["name"];?>
            </td>
            <td><?php echo $v["price_out"];?></td>
            <td><?php echo $v["saldo"];?></td>
            <td><?php echo $v["category"];?></td>
        </tr>
<?php }?>
        </tbody>
    </table>
</div>
 </div>
 </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>