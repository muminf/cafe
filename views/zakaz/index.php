<title>Столики</title>
<?php include ROOT . '/views/layouts/header.php'; 
$busyList = Zakaz::getBusyStoli();
$busyList = (is_array($busyList) ? $busyList : array());
?>
<meta name="refresh" http-equiv="refresh" content="10">
<div class="row">
    <div class="col-md-2" style="margin: 10px">
        <div class="input-group mb-2">
             <button type="button" id="newSsoboy" class="btn btn-block btn-info" data-toggle="modal" data-target="#addNewSsoboy">
                 <span style="float:left"><i class="fa fa-plus">&nbsp;</i></span>Новый заказ с собой
             </button>
         </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addNewSsoboy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Новый заказ с собой</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="newSsoboyForm" name="newSsoboyForm" action="/shop/zakaz/AddNewSsoboy" method="post">
                <div class="form-group">
                    <input name="client" autocomplete="off" required="true" minlength="2" maxlength="30" placeholder="Имя и номер клиента" class="form-control">
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>
           </div> 
        </div>
    </div>
  </div>
</div>
<div class="row">
    <!-- Столики -->
    <div class="col-md-6">
        <?php foreach ($list[Zakaz::stolik] as $k=>$v){
        if(array_key_exists($k,$busyList)){
            $clr = "#f40202";
            $v["oficiant"] = $busyList[$k]["oficiant"];
        }else{
            $clr = "#37cd49";
        }
        ?>
        <div class="row">
            <div class="col-md-5">
                <div class="card card-info">
                    <div class="card-header" style="background-color:<?php echo $clr?>">
                        <h3 style="display:inline" class="card-title"><?php echo $v["name"] ?></h3>
                    </div>
                    <div class="card-body ">
                        <p>Официант: <?php echo $v["oficiant"]?></p>
                        <p>Сид.мест: <?php echo $v["chair_count"]?></p>
                        <a class="btn btn-primary" href="/shop/zakaz/stol/<?php echo $v["id"] ?>" role="button">Перейти</a>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
    <!-- Кабины -->
    <div class="col-md-6">
        <?php foreach ($list[Zakaz::kabina] as $k=>$v){
        if(array_key_exists($k,$busyList)){
            $clr = "#f40202";
            $v["oficiant"] = $busyList[$k]["oficiant"];
        }else{
            $clr = "#37cd49";
        }
        ?>
        <div class="row">
            <div class="col-md-5">
                <div class="card card-info">
                    <div class="card-header" style="background-color:<?php echo $clr?>">
                        <h3 style="display:inline" class="card-title"><?php echo $v["name"] ?></h3>
                    </div>
                    <div class="card-body ">
                        <p>Официант: <?php echo $v["oficiant"]?></p>
                        <p>Сид.мест: <?php echo $v["chair_count"]?></p>
                        <a class="btn btn-primary" href="/shop/zakaz/stol/<?php echo $v["id"] ?>" role="button">Перейти</a>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
    
</div>
<div class="row">
    <div class="col-md-6">
        <?php foreach($sSoboyList as $k=>$v){ ?>
            <div class="row">
                <div class="col-md-5">
                    <div class="card card-info">
                        <div class="card-header" style="background-color:#ffc800">
                            <h3 style="display:inline;color:#000000" class="card-title"><?php echo $v["client"] ?></h3>
                        </div>
                        <div class="card-body ">
                            <p>Официант: <?php echo $v["oficiant"]?></p>
                            <a class="btn btn-primary" href="/shop/zakaz/Ssoboy/<?php echo $v["id"] ?>" role="button">Перейти</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


<?php include ROOT . '/views/layouts/footer.php'; ?>