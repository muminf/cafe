<title>Кухня</title>
<?php include ROOT . '/views/layouts/header.php'; 
   #Utils::pre(Spr::getByCategory(1));
   #Utils::pre(Utils::getList(Agent::tbl_name,"id,name","id_type=".Agent::postavshik));
#Utils::pre($list);
#echo Utils::getLastId(Material::tbl_name);
#echo $_SESSION["zakazCount"]." -> ".count($list);
?>
<?php 
    if($_SESSION["zakazCount"] < count($list)){
        $_SESSION["zakazCount"] = count($list); 
?>    
    <audio src="/shop/template/audio/newZakaz.mp3" volume="1" autoplay=""></audio>
<?php } ?>
<meta http-equiv="refresh" content="10">
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-4">
            <h1>Кухня -> Заказы</h1>
        </div>
        <div class="col-sm-4">
            <h1><?php echo date("d.m.Y")?></h1>
        </div>
            
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row"> 
            <div class="col-md-12">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Список заказов</h3>
               <h3 style="display:block;float:right;" class="card-title"></h3>
             </div>
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="zakazListTable">
                 <tr>
                   <th>Наименование</th>
                   <th>Количество</th>
                   <th>Отменено</th>
                 <!--  <th>Готово</th> -->
                   <th>Стол</th>
                   <th>Официант</th>
                   <th></th>
                   <th style="width:10px"></th>
                 </tr>
                <?php #unset($_SESSION["prihodList"]); 
                    foreach($list as $k=>$v){ 
                        $rowColor = ($v["canceled_count"]>0 ? "#e6a8a8" : "");
                ?>
                 <tr style="background-color:<?php echo $rowColor ?>" id="<?php echo $k?>" class="list">
                   <td><?php echo $v["product"] ?></td>
                   <td><?php echo $v["count"] ?></td>
                   <td><?php echo $v["canceled_count"] ?></td>
                 <!--   <td>
                       <a href="/shop/zakaz/MinusFromReadyList/<?php echo $k?>" class="MinusFromReadyList">
                            <button type='button' class='btn btn-block btn-warning btn-sm counter'>-</button>
                        </a> 
                            <div class="ready"><?php echo $v["count"] ?></div>
                        <a href="/shop/zakaz/AddToReadyList/<?php echo $k?>" class="AddToReadyList">
                            <button type='button' class='btn btn-block btn-warning btn-sm counter'>+</button>
                        </a> 
                   </td> -->
                   <td><?php echo $v["stol"] ?></td>
                   <td><?php echo $v["oficiant"] ?></td>
                   <td>
                        <a href="/shop/zakaz/SendFromKitchen/<?php echo $k?>" class="SendFromKitchen">
                            <button type='button' class='btn btn-block btn-warning btn-sm'>Отправить</button>
                        </a>
                   </td>
                   <td></td>
                 </tr>
                <?php } ?>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->
         </div>
      </div>
      </div>
     </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>