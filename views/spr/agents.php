<title>Контрагенты</title>
<?php include ROOT . '/views/layouts/header.php'; 
#Utils::pre(Utils::getList(Spr::tbl_name, "id,name"));
?>
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-4">
            <h1>Контрагенты</h1>
        </div>
        <div class="col-md-3">
            <!-- Button trigger modal -->
            <div class="input-group mb-3">
                <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#addNewAgent">
                    <span style="float:left"><i class="fa fa-plus">&nbsp;</i></span> Добавить нового контрагента
                </button>
            </div>
        </div>
        <div class="col-sm-5">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/spr/">Настройки</a></li>
                <li class="breadcrumb-item"><a href="/shop/spr/Stoliki">Столики</a></li>
                <li class="breadcrumb-item"><a href="/shop/spr/Agents">Контрагенты</a></li>
                <li class="breadcrumb-item"><a href="/shop/spr/Users">Пользователи</a></li>
            </ol>
        </div>
            
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row"> 
            <div class="col-md-12">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Значения</h3>
             </div>

          
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
                 <form action="/shop/spr/EditAgentExec" method="POST">
               <table class="table table-hover" id="sprListTable">
                 <tr>
                   <th>ID</th>
                   <th>Наименование</th>
                   <th>Тел</th>
                 </tr>
                <?php #unset($_SESSION["prihodList"]);   
                    if(isset($list)){
                    foreach($list as $k=>$v){ 
                ?>
                 
                 <tr id="r<?php echo $v["id"]?>" class="list">
                     
                    <td><?php echo $k ?></td>
                    <td>
                        <input type="text" required="true"  name="name[<?php echo $k ?>]" maxlength="20" value="<?php echo $v["name"] ?>">
                    </td>
                    <td>
                        <input type="text" class="phone" name="phone[<?php echo $k ?>]" maxlength="20" value="<?php echo $v["phone"] ?>">
                    </td>
                 </tr>
                 
                <?php }} ?>
                <tr>
                    <td colspan="4">
                        <div class="col-md-2">
                           <button  type="submit" id="<?php echo $id ?>" class="btn btn-block btn-info updateProductPrice">Сохранить</button>
                        </div>
                    </td>
                </tr>
               </table>
                     </form>
               
             </div>
             <!-- /.card-body -->
           </div>
                
           <!-- /.card -->
         </div>
         </div>
         
         </div>

     </div><!-- /.container-fluid -->
   </section>
   <!-- Modal -->
<div class="modal fade" id="addNewAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Добавление нового агента</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="addNewAgent" name="newSprValForm" action="/shop/spr/AddNewAgent" method="post">
                <div class="form-group">
                    <input name="name" autocomplete="off" id="agent_name" required="true" minlength="2" placeholder="Наименование" class="typeahead form-control">
                    <input type="hidden" name="id">
                </div>
                <div class="form-group">
                    <input name="phone" autocomplete="off" class="phone" required="true" minlength="1" placeholder="Номер телефона" class="form-control">
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>


           </div> 
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
<?php include ROOT . '/views/layouts/footer.php'; ?>