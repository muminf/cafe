<title>Настройки столиков</title>
<?php include ROOT . '/views/layouts/header.php'; 
#Utils::pre(Utils::getList(Spr::tbl_name, "id,name"));
?>
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-4">
            <h1>Столики</h1>
        </div>
        <div class="col-md-3">
            <!-- Button trigger modal -->
            <div class="input-group mb-3">
                <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#addNewDish">
                    <span style="float:left"><i class="fa fa-plus">&nbsp;</i></span> Добавить новый столик
                </button>
            </div>
        </div>
        <div class="col-sm-5">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/spr/">Настройки</a></li>
                <li class="breadcrumb-item"><a href="/shop/spr/Stoliki">Столики</a></li>
                <li class="breadcrumb-item"><a href="/shop/spr/Agents">Контрагенты</a></li>
                <li class="breadcrumb-item"><a href="/shop/spr/Users">Пользователи</a></li>
            </ol>
        </div>
            
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row"> 
            <div class="col-md-12">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Значения</h3>
             </div>

          
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
                 <form action="/shop/spr/EditStolikiExec" method="POST">
               <table class="table table-hover" id="sprListTable">
                 <tr>
                   <th>ID</th>
                   <th>Наименование</th>
                   <th>Количество мест</th>
                   <th>Тип</th>
                   <th></th>
                 </tr>
                <?php #unset($_SESSION["prihodList"]);   
                    if(isset($list)){
                    foreach($list as $k=>$v){ 
                ?>
                 
                 <tr id="r<?php echo $v["id"]?>" class="list">
                     
                    <td><?php echo $k ?></td>
                    <td>
                        <input type="text" name="name[<?php echo $k ?>]" maxlength="30" value="<?php echo $v["name"] ?>">
                    </td>
                    <td>
                        <input type="text" name="chair_count[<?php echo $k ?>]" value="<?php echo $v["chair_count"] ?>">
                    </td>
                    <td>
                        <select name="id_type[<?php echo $k ?>]" id="id_type" data-placeholder="Тип столика" class="form-control select2" style="width: 100%;">
                            <?php echo Utils::getOptionList(Spr::getByCategory(Spr::seat_type),$v["id_type"])?>                     
                        </select>
                    </td>
                 </tr>
                 
                <?php }} ?>
                <tr>
                    <td colspan="4">
                        <div class="col-md-2">
                           <button  type="submit" id="<?php echo $id ?>" class="btn btn-block btn-info updateProductPrice">Сохранить</button>
                        </div>
                    </td>
                </tr>
               </table>
                     </form>
               
             </div>
             <!-- /.card-body -->
           </div>
                
           <!-- /.card -->
         </div>
         </div>
         
         </div>

     </div><!-- /.container-fluid -->
   </section>
   <!-- Modal -->
<div class="modal fade" id="addNewDish" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Добавление нового столика</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="addSprVal" name="newSprValForm" action="/shop/spr/AddNewStolik" method="post">
                <div class="form-group">
                    <input name="name" autocomplete="off" id="product_name" required="true" minlength="2" placeholder="Наименование" class="typeahead form-control">
                </div>
                <div class="form-group">
                    <select name="id_type" id="id_type" data-placeholder="Тип столика" class="form-control select2" style="width: 100%;">
                       <option></option>
                       <?php echo Utils::getOptionList(Spr::getByCategory(Spr::seat_type),$id)?>                     
                    </select>
                </div>
                <div class="form-group">
                    <input name="chair_count" autocomplete="off" id="chair_count" required="true" minlength="1" placeholder="Количество сидячих мест" class="form-control">
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>


           </div> 
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
<?php include ROOT . '/views/layouts/footer.php'; ?>