<?php if(User::isGuest()){header("Location:/shop/user/login");}?>
<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Система управления кафе</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="/shop/template/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="/shop/template/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="/shop/template/dist/css/adminlte.min.css">
      <!-- iCheck -->
      <link rel="stylesheet" href="/shop/template/plugins/iCheck/square/blue.css" >
      <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="/shop/template/css/fonts1.css" >
      
      
      <!-- Select2 -->
      <link rel="stylesheet" href="/shop/template/plugins/select2/select2.min.css">
      
      <link rel="stylesheet" href="/shop/template/css/style.css">
      
      <link rel="stylesheet" href="/shop/template/plugins/datepicker/datepicker3.css">
      
      <link rel="stylesheet" href="/shop/template/plugins/datatables/jquery.dataTables.min.css">
      
      <link rel="stylesheet" href="/shop/template/css/bootstrap-toggle.min.css">
      
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">
  
<?php #include_once 'navbar.php';?>
<?php include_once 'left_menu.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- /.content -->
 
 