<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="/shop/template/dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">shop 1.0</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/shop/template/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <p class="d-block" style="color:#ffffff;margin-top:-10px;"><?php echo $_SESSION["user"]["user_fio"]; ?></a>
           <a href="/shop/user/logout" class="d-block" style="margin-top:8px">
              <i class="nav-icon fa fa-lock"></i>  Выйти
           </a>
          <a href="/shop/user/ChangePassword" class="d-block" style="margin-top:8px">
              <i class="nav-icon fa fa-edit"></i>  Изменить пароль
           </a>
        </div>
      </div><?php
        #Utils::pre($_SESSION['user']);
      ?>
      <?php if($_SESSION["user"]["user_type"] == User::admin){?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/shop/sklad" class="nav-link">
              <i class="nav-icon fa fa-home"></i>
              <p>
                Склад
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/shop/sklad/new" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Приход</p>
                </a>
              </li>
              <li class="nav-item">
                  <a href="/shop/sklad/list/<?php echo date("d-m-Y")?>/<?php echo date("d-m-Y")?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Накладные</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/sklad/saldo" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Остаток</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/sklad/spisat" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Списание материалов</p>
                </a>
              </li>
            </ul>
              
          </li>
        </ul>
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/shop/menu" class="nav-link">
              <i class="nav-icon fa fa-cutlery"></i>
              <p>
                Меню
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/shop/menu/menu" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Блюда</p>
                </a>
              </li>
              <li class="nav-item">
                  <a href="/shop/menu/sostav/" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Состав блюд</p>
                </a>
              </li>
              <li class="nav-item">
                  <a href="/shop/menu/saldo/" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Добавить готовые блюда</p>
                </a>
              </li>
              <li class="nav-item">
                  <a href="/shop/menu/spisat/" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Списание блюд</p>
                </a>
              </li>
            </ul>
          </li>
          
        </ul>
           <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/shop/zakaz" class="nav-link">
              <i class="nav-icon fa fa-list"></i>
              <p>
                Заказы
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/shop/zakaz" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Столики</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/zakaz/kitchen/" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Кухня</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/zakaz/bar/" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Бар</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/kassa/" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Касса</p>
                </a>
              </li>
            </ul>
          </li>
          
        </ul>
           <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/shop/zakaz" class="nav-link">
              <i class="nav-icon fa fa-cogs"></i>
              <p>
                Справочники
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/shop/spr/" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Настройки</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/spr/Stoliki" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Столики</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/spr/Agents" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Контрагенты</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/shop/spr/Users" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Пользователи</p>
                </a>
              </li>
            </ul>
          </li>
          
        </ul>
           <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/shop/report" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Отчеты
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/shop/report/Cash/<?php echo date("d-m-Y")?>/<?php echo date("d-m-Y")?>" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Касса</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
      <?php } ?>
    </div>
    <!-- /.sidebar -->
  </aside>


