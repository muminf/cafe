 </div>
  <!-- /.content-wrapper -->
<!--<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0
    </div>
    <strong>Система управления торговлей &copy; 2018 <a href="#">shop</a>.</strong> Все права защищены.
  </footer>-->

 
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/shop/template/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/shop/template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- 
<script src="/shop/template/plugins/fastclick/fastclick.js"></script>FastClick -->
<!-- AdminLTE App -->
<script src="/shop/template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo 
<script src="/shop/template/dist/js/demo.js"></script>purposes -->
<!-- select2 -->
<script src="/shop/template/plugins/select2/select2.full.min.js"></script>

<script src="/shop/template/js/typeahead.js"></script>
<script src="/shop/template/js/jquery.autocomplete.min.js"></script>

<script src="/shop/template/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/shop/template/plugins/datepicker/bootstrap-datepicker.ru.js"></script>
<script src="/shop/template/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/shop/template/js/bootstrap-toggle.min.js"></script>
<script src="/shop/template/plugins/input-mask/jquery.inputmask.js"></script>

<script>
$(document).ready(function() {
    $("#shcode").keypress(function(e){
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if(keycode == '13'){
            console.log(keycode);
            var shcode = $(this).val();
            getByShCode(shcode);
        }
    });
    
    function getByShCode(shcode){
        $.get("/shop/kassa/getByShCode/"+shcode,{},function(res){
            res = $.parseJSON(res);
            if(typeof res.product_id != "undefined"){
                addToCartShCode(res);
            }
        });
    }
    $(".select2").select2();
    $('#product').typeahead({
        source: function(query, result)
        {
          objects = [];
            map = {};
            var data;
         $.ajax({
          url:"/shop/kassa/getByName/"+query,
          method:"POST",
          data:null,
          dataType:"json",
          success:function(data)
          {
            $.each(data, function(i, object) {
                map[object.product_name] = object;
                objects.push(object.product_name);
             });
            result(objects);
          }
         })
        },
        updater: function(item) {
            addToCart(item);
            return item;
        }
    });
    
    
    var cart = [];
    var allSum = 0;
    
    function enableSaleButton(){
        if(Object.keys(cart).length < 1){
            $("#closeSaleButton").attr("disabled","true");
        }else{
            $("#closeSaleButton").removeAttr("disabled");
        }
    }
    
    enableSaleButton();
    
    function addToCartShCode(item){
        //console.log(map[item].product_id + map[item].product_name + map[item].product_count);
        if(typeof cart[item.product_id] == "undefined"){
            item.product_count = 1;
            cart[item.product_id] = item;
            /*cart[item.product_id] = {
                product_id : item.product_id,
                product_count : 1,
                product_name : item.product_name,
                product_shtrixkod : item.product_shtrixkod,
                product_price_sell  : item.product_price_sell,
                product_saldo   :   item.product_saldo
            };*/
            //console.log(cart);
            //cart[item.product_id].product_id = item.product_id;
            $("#saleList tr:last").after("<tr id='"+item.product_id+"'>"
                                        +"<td>"+item.product_shtrixkod+"</td>"
                                        +"<td>"+item.product_name+"</td>"
                                        +"<td class='cnt'>"+item.product_count+"</td>"
                                        +"<td>"+item.product_price_sell+"</td>"
                                        +"<td>"+item.product_price_sell+"</td>"
                                        +"<td>"+item.product_saldo+"</td>"
                                        +"<td><input type='button' class='del' id='"+item.product_id+"' value='Удалить'></td>"
                                    +"</tr>");
        }else{
            cart[item.product_id].product_count++;
            summa = Math.round(parseFloat(cart[item.product_id].product_count)*parseFloat(cart[item.product_id].product_price_sell));
            cart[item.product_id].product_saldo--;
            
            $("#saleList tr#"+item.product_id+" td:nth-child(3)").text(cart[item.product_id].product_count);
            $("#saleList tr#"+item.product_id+" td:nth-child(5)").text(summa.toFixed(2));
            $("#saleList tr#"+item.product_id+" td:nth-child(6)").text(cart[item.product_id].product_saldo);
            
        }
        allSum += parseFloat(item.product_price_sell);
       // console.log('----');
        //console.log(cart);
         enableSaleButton();
    }
    
     $(document).on('click', '.dropdown-item', function(e){
        $("#product").val("");
     });
    function addToCart(item){
        //console.log(map[item].product_id + map[item].product_name + map[item].product_count);
        if(typeof cart[map[item].product_id] == "undefined"){
            $("#product").val("");
            map[item].product_count = 1;
            cart[map[item].product_id] = map[item];
            $("#saleList tr:last").after("<tr id='"+map[item].product_id+"'>"
                                        +"<td>"+map[item].product_shtrixkod+"</td>"
                                        +"<td>"+map[item].product_name+"</td>"
                                        +"<td class='cnt'>"+map[item].product_count+"</td>"
                                        +"<td>"+map[item].product_price_sell+"</td>"
                                        +"<td>"+map[item].product_price_sell+"</td>"
                                        +"<td>"+map[item].product_saldo+"</td>"
                                        +"<td><input type='button' class='del' id='"+map[item].product_id+"' value='Удалить'></td>"
                                    +"</tr>");
        }else{
            cart[map[item].product_id].product_count++;
            cart[map[item].product_id].product_saldo--;
            summa = Math.round(parseFloat(cart[map[item].product_id].product_count)*parseFloat(cart[map[item].product_id].product_price_sell));
            $("#saleList tr#"+map[item].product_id+" td:nth-child(3)").text(cart[map[item].product_id].product_count);
            $("#saleList tr#"+map[item].product_id+" td:nth-child(5)").text(summa.toFixed(2));
            $("#saleList tr#"+map[item].product_id+" td:nth-child(6)").text(cart[map[item].product_id].product_saldo);
        }
        allSum += parseFloat(map[item].product_price_sell);
        console.log('----');
        console.log(cart);
        enableSaleButton();
    }
    /*
    $(document).bind('beforeunload',function(){
        return false;
        if(Object.keys(cart).length > 0){
            return false;
        }
    });
    
    window.addEventListener("beforeunload", function (event) {
        // Cancel the event as stated by the standard.
        event.preventDefault();
        // Chrome requires returnValue to be set.
        event.returnValue = '';
        
        return "";
    });*/
    
    $(window).bind({
        beforeunload: function(ev) {
            ev.preventDefault();
        },
        unload: function(ev) {
            ev.preventDefault();
        }
    });
    
    
    $(document).on('click', '.del', function(e){
        var id = $(this).attr("id");
        $.post("/shop/kassa/DelFromCart/",{'arr' : JSON.stringify(cart[id])},function(data){
            if(data > 0){
                $("tr#"+id).remove();
                $("#product").val("");
                if(Object.keys(cart).length < 2){
                    $("#closeSaleButton").attr("disabled","true");
                }else{
                    $("#closeSaleButton").removeAttr("disabled");
                }
                delete cart[id];
                var sum = parseFloat($("#saleList tr#"+id+" td:nth-child(5)").text());
                allSum -= sum;
            }
        });
        
        /*
        if(Object.keys(cart).length < 1){
            $("#closeSaleButton").attr("disabled","true");
        }else{
            $("#closeSaleButton").removeAttr("disabled");
        }
        enableSaleButton();*/
    });
    
    $(document).on('click', '.cnt', function(e){
       var id = $(this).parent("tr").attr("id");
       //cart[id].product.count++;
       item = cart[id];
       addToCartShCode(item);
    });
    
    $("#pay").click(function(){
        var printSale = ($("#printSale").is(':checked') ? 1 : 0);
        var paySum = $("input[name=paySum]").val();
        var worker = $("#worker").val();
        $.post("/shop/kassa/CloseSale/",{'cart' : JSON.stringify(cart),'printSale' : printSale,'paySum' : paySum,'worker' : worker},function(){});
    });
    
    $(".closeSaleModal").click(function(){
        $("#saleSum").text(allSum.toFixed(2));
    });
    
    function calc_sdacha(){
        var paySum = parseFloat($("input[name=paySum]").val());
        var sdacha = 0;
        if(paySum >= allSum || parseInt($("#worker").val()) > 0){
            sdacha = (paySum-allSum).toFixed(2);
            $("#pay").removeAttr("disabled");
        }else{
            $("#pay").attr("disabled","true");
        }
        $("#sdacha").text(sdacha);
    }
    
    $("#worker").change(function(){
        calc_sdacha();
    });
    
    $("input[name=paySum]").keyup(function(){
        calc_sdacha();
    });
    
    $("#rec_type").change(function(){
        if($(this).prop('checked')){
            $("#rec_worker").hide();
            $("#rec_firma").show();
            console.log('checked');
        }else{
            console.log('not checked');
            $("#rec_worker").show();
            $("#rec_firma").hide();
        }
    });
    /*$(window).bind('beforeunload', function(e) { 
        
        e.preventDefault();
        return false;
        
        
    });*/
    $("#worker").change(function(){
        $("#firma").val(0);
        $('#firma').trigger('change.select2');
    });
    
    $("#firma").change(function(){
        $("#worker").val(0);
        $('#worker').trigger('change.select2');
    });
});
</script>
</body>
</html>

