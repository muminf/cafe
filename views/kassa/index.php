<title>Касса</title>
<?php include ROOT . '/views/layouts/header.php';
#Utils::pre($_SESSION["cart"]);

/*$db = Db::getConnection();
$result = $db->query($sql);
$list = array();
$products = "";
if($result){
     while($r = $result->fetch(PDO::FETCH_ASSOC)){
        # $list["product_id"] = $r["product_name"];
         $products .= "<option value='".$r["product_id"]."'>".$r["product_name"];
     }
}*/

?>
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
        
         <div class="row" style="margin-bottom:5px">
             <div class="col-md-3">
                 <input id="shcode" name="shcode" placeholder="Штрихкод" class="form-control" autocomplete="off">
             </div>
             
             <div class="col-md-5">
                <!-- <select name="product" id="product" data-placeholder="Наименование" class="form-control select2" style="width: 100%;">
                       <option></option>
                       <?php #echo $products ?>                     
                </select> -->
                 <input name="product" placeholder="Наименование" id="product" class="form-control">
             </div>
             <div class="col-md-2">
                    <input type="button"  id="closeSaleButton" class="btn btn-block btn-info closeSaleModal form-control" data-toggle="modal" data-target="#closeSaleModal" value="Закрыть">
              </div>  
             <div class="col-md-2">
                    <input type="button"  id="addRashodButton" class="btn btn-block btn-info form-control" data-toggle="modal" data-target="#addRashodModal" value="Расход средств">
              </div> 
               
         </div>    
                  <div class="row">
              <div class="col-md-12">
                   <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="saleList">
                 <thead>
                   <tr>
                   <th>Штрихкод</th>
                   <th>Наименование</th>
                   <th>Количество</th>
                   <th>Цена</th>
                   <th>Сумма</th>
                   <th>Остаток</th>
                   <th></th>
                 </tr>
                 </thead>
                 <tbody>
                <?php #unset($_SESSION["prihodListShow"]);   
                    if(isset($list)){
                    foreach($list as $k=>$v){ 
                        $v["sum_out"] = (is_null($v["sum_out"]) ? 0 : $v["sum_out"]);
                ?>
                    <tr class="list <?php echo ($v["id"] == $id ? "selected" : "")?>">
                    <td><?php echo $v["id"] ?></td>
                    <td><?php echo $v["stol"].$v["client"] ?></td>
                    <td><?php echo $v["oficiant"] ?></td>
                    <td><?php echo $v["sum_out"] ?></td>
                    <td><?php echo $v["beg_time"] ?></td>
                    <td><a class="btn btn-info closeSale" id="<?php echo $v["id"] ?>" href="/shop/kassa/PrintCash/<?php echo $v["id"] ?>">Чек</a></td>
                    <td><a class="btn btn-info closeSale" id="<?php echo $v["id"] ?>" href="/shop/kassa/CloseSale/<?php echo $v["id"] ?>" role="button" data-toggle="modal" data-target="#closeSale">Закрыть</a></td>
                 </tr>
                <?php }} ?>
                 </tbody>
               </table>
             </div>
             
               <!-- Modal -->
            <div class="modal fade" id="closeSaleModal" tabindex="-1" role="dialog"  aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                 <form id="closeSaleForm" name="closeSaleForm" action="/shop/kassa/CloseSale" method="post">
                    <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-usd"></i> Оплата</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-info">
                            <div class="card-body">
                                <h2><p style="display:inline" id="stol"></p></h2>
                                <h2><p style="display:inline">Сумма покупки:</p><p style="display:inline" id="saleSum"></p></h2>
                                <h2><p style="display:inline">Оплачено:<input style="width:150px;display:inline;margin-left:5px" autocomplete="off" class="form-control" name="paySum" ></h2>
                                <h2><p style="display:inline">Сдача:</p>  <p style="display:inline" id="sdacha"></p></h2>
                                <select name="worker" id="worker" data-placeholder="Выберите сотрудника" class="form-control" style="width: 100%;">
                                    <option value="0"></option>
                                    <?php echo Utils::getOptionList($users,0,"user_fio")?>                     
                                </select>
                            <!-- /.card-body -->
                                <div class="form-group">
                                    <h3 style="display:inline">Распечатать чек:</h3>
                                    <input style="display:inline" type="checkbox" id="printSale" name="printSale" data-toggle="toggle" data-size="large" data-on="Да" data-off="Нет">
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary" disabled="true" id="pay">Оплатить</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
             <!-- /.card-body -->
             
               <!-- Modal -->
            <div class="modal fade" id="addRashodModal" tabindex="-1" role="dialog"  aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                 <form id="addRashodForm" name="addRashodForm" action="/shop/kassa/AddRashod" method="post">
                    <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-usd"></i>Расход средств</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-info">
                            <div class="card-body">
                                <h2><p style="display:inline" id="stol"></p></h2>
                                <h2><p style="display:inline">Сумма:<input style="width:150px;display:inline;margin-left:5px;" autocomplete="off" class="form-control" name="sum_out" ></h2>
                                <div class="form-group">
                                    <h3 style="display:inline">Получатель:</h3>
                                    <input style="display:inline" type="checkbox" id="rec_type" name="rec_type" data-toggle="toggle" data-size="large" data-on="Фирма" data-off="Сотрудник" checked="checked">
                                </div>
                                
                                <select name="firma" id="rec_firma" data-placeholder="Выберите фирму" class="form-control" style="width: 100%;">
                                    <option value="0"></option>
                                    <?php echo Utils::getOptionList($firma)?>
                                </select>
                                
                                <select name="worker" id="rec_worker" data-placeholder="Выберите сотрудника" class="form-control" style="width:100%;display:none">
                                    <option value="0"></option>
                                    <?php echo Utils::getOptionList($users,0,"user_fio")?>                     
                                </select>
                                <br>
                                <textarea name="nazn" cols="58" rows="4" placeholder="Назначение платежа"></textarea>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Оплатить</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
             <!-- /.card-body -->
             
           </div>
              </div>
          </div>
          
          <!-- Modal -->
<div class="modal fade" id="closeSaleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Добавление нового блюда</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="newRashodForm" name="newRashodForm" action="/shop/kassa/AddNewRashod" method="post">
                <div class="form-group">
                    <input name="sum_out" autocomplete="off" id="sum_out" required="true" minlength="1" placeholder="Сумма" class="form-control">
                </div>
                <div class="form-group">
                    <select name="id_agent" id="id_agent" placeholder="Агент" class="form-control select2">
                        <?php echo Utils::getOptionList(Utils::getList(Agent::tbl_name,"id,name","status=".Spr::status_active))?>                    
                    </select>
                </div>
                <div class="form-group">
                    <textarea name="nazn" placeholder="Назначение платежа" class="form-control" id="nazn"></textarea>
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>


           </div> 
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
     </div><!-- /.container-fluid -->
   </section>
   <style>
       .dataTables_filter{
           height: 44px;
           font-size: 1.2em;
       }
       #worker option{
            padding: 8px;
            font-size: 2em;
       }
   </style>    
<?php include ROOT . '/views/layouts/footer.php'; ?>