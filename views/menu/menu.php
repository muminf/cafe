<title>Блюда</title>
<?php include ROOT . '/views/layouts/header.php';
if(!isset($_SESSION["edit_product"])){
    $_SESSION["edit_product"] = 0;
} 
?>
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-4">
            <h1>Список меню</h1>
        </div>
        <div class="col-md-3">
            <!-- Button trigger modal -->
            <div class="input-group mb-3">
                <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#addNewDish">
                    <span style="float:left"><i class="fa fa-plus">&nbsp;</i></span> Добавить новое блюдо
                </button>
            </div>
        </div>
        <div class="col-sm-5">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/menu/menu">Блюда</a></li>
                <li class="breadcrumb-item"><a href="/shop/menu/sostav">Состав</a></li>
                <li class="breadcrumb-item"><a href="/shop/menu/saldo">Остаток блюд</a></li>
                <li class="breadcrumb-item"><a href="/shop/menu/spisat">Списать</a></li>
            </ol>
        </div>
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row">
              <div class="col-md-9" style="height:100%">
                   <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="menuList">
                 <thead>
                   <tr>
                   <th>ID</th>
                   <th>Наименование</th>
                   <th>Категория</th>
                   <th>Остаток</th>
                   <th>Цех</th>
                   <th>Себистоимость</th>
                   <th>Цена</th>
                   <th>Доход</th>
                   <th>Статус</th>
                 </tr>
                 </thead>
                 <tbody>
                <?php #unset($_SESSION["prihodListShow"]);   
                    if(isset($list)){
                    foreach($list as $k=>$v){ 
                ?>
                <tr class="list">
                    <td><?php echo $v["id"] ?></td>
                    <td><?php echo $v["name"] ?></td>
                    <td><?php echo $v["category"] ?></td>
                    <td><?php echo $v["saldo"] ?></td>
                    <td><?php echo $v["factory"] ?></td>
                    <td><?php echo $v["price_in"] ?></td>
                    <td><?php echo $v["price_out"] ?></td>
                    <td><?php echo $v["income"] ?></td>
                    <td><?php echo $v["status_text"] ?></td>
                </tr>
                <?php }} ?>
                 </tbody>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
              </div>
              <div class="col-md-3">   
             <form action="/shop/menu/EditProductItem" method="post" id="editProductFrom">
              <div class="card card-info">
                  <div class="card-header">
               <h3 style="display:inline" class="card-title">Свойства</h3>
             </div>
             <div class="card-body ">
                <div class="form-group">
                     <input name="name" autocomplete="off" id="product_name" required="true" minlength="2" class="typeahead form-control" placeholder="Наименование">
                </div>
                <div class="form-group">
                    <select name="id_category" id="product_id_category" data-placeholder="Категория" class="form-control" style="width: 100%;"  aria-hidden="true">
                       <option></option>
                       </div><?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_category));?>
                    </select>
                </div>
                
                 <div class="form-group">
                    <select name="id_factory" id="id_factory" data-placeholder="Цех" class="form-control" style="width: 100%;"  aria-hidden="true">
                       </div><?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_factory));?>
                    </select>
                 </div>
                 <div class="form-group">
                     <input name="price_in" type="text" class="form-control" autocomplete="off" required="true" minlength="1" id="price_in" placeholder="Приход">
                 </div>
                 <div class="form-group">
                     <input name="price_out" type="text" class="form-control" autocomplete="off" required="true" minlength="1" id="price_out" placeholder="Продажа">
                 </div>
                 <div class="form-group">
                     <select name="status" id="status" data-placeholder="Активность" class="form-control" style="width: 100%;"  aria-hidden="true">
                        </div><?php echo Utils::getOptionList(Spr::getByCategory(Spr::status));?>
                    </select>
                 </div>
            <!--     <div class="form-group">
                     <input name="coock_time" type="text" class="form-control" required="true" minlength="1" id="coock_time" placeholder="Время приготовления">
                 </div> -->
                 <button type="submit" class="btn btn-block btn-info">Сохранить</button>
             <!-- /.card-body -->
             </div>


           </div> 
            </form>

          </div>
          </div>
        <!-- Modal -->
<div class="modal fade" id="addNewDish" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Добавление нового блюда</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="addNewDish" name="newDishForm" action="/shop/menu/AddNewDish" method="post">
                <div class="form-group">
                    <input name="name" autocomplete="off" id="product_name" required="true" minlength="2" placeholder="Наименование" class="typeahead form-control">
                    <input name="id" id="id" type="hidden">
                </div> 
                <div class="form-group">
                    <input type="text" name="price_in" autocomplete="off" id="price" class="form-control" placeholder="Цена покупки">
                </div> 
                <div class="form-group">
                    <input type="text" name="price_out" autocomplete="off" id="price" class="form-control" placeholder="Цена продажи">
                </div>
                <div class="form-group">
                    <select name="id_category" id="id_category" placeholder="Категория" class="form-control">
                        <?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_category))?>                    
                    </select>
                </div>
                    
                <div class="form-group">
                    <select name="id_factory" id="id_factory" placeholder="Цех" class="form-control">
                        <?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_factory))?>                    
                    </select>
                </div>
                <div class="form-group">
                    <input autocomplete="off" name="coock_time" type="text" placeholder="Время приготовления" class="form-control" required="true" minlength="1" id="coock_time">
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>


           </div> 
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary">Сохранить</button>
      </div>
    </div>
  </div>
</div>
     </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>