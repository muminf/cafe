<title>Состав блюд</title>
<?php include ROOT . '/views/layouts/header.php'; 
   #Utils::pre(Spr::getByCategory(1));
   #Utils::pre(Utils::getList(Agent::tbl_name,"id,name","id_type=".Agent::postavshik));
#Utils::pre($_SESSION["product"]);
#echo Utils::getLastId(Material::tbl_name);

#$sostavList = Product::getSostav($id);
#Utils::pre($sostavList);
?>
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-2">
            <h1>Меню</h1>
        </div>
        <div class="col-md-3">
            <!-- Button trigger modal -->
                <select name="product" id="product" data-placeholder="Выберите блюдо" class="form-control select2" style="width: 100%;">
                       <option></option>
                       <?php echo Utils::getOptionList(Utils::getList(Product::tbl_name, "id,name"), $id)?>                     
                </select>
        </div>
        <div class="col-md-2">
            <!-- Button trigger modal -->
            <div class="input-group mb-3">
                <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#addNewDish">
                    <span style="float:left"><i class="fa fa-plus">&nbsp;</i></span> Добавить новое блюдо
                </button>
            </div>
        </div>
        <div class="col-sm-5">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/menu/menu">Блюда</a></li>
                <li class="breadcrumb-item"><a href="/shop/menu/sostav">Состав</a></li>
                <li class="breadcrumb-item"><a href="/shop/menu/saldo">Остаток блюд</a></li>
                <li class="breadcrumb-item"><a href="/shop/menu/spisat">Списать</a></li>
            </ol>
        </div>
            
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row"> 
            <div class="col-md-9">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Состав блюда</h3>
               <h4 style="display:inline;float:right" class="card-title">Сумма: <span id="allSum"><?php echo $allSum; ?></span>с</h4>
             </div>

          <form action="/shop/sklad/filter" method="POST" id="newProductForm">
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="sostavListTable">
                 <tr>
                   <th>Наименование</th>
                   <th>Количество</th>
                   <th>Цена</th>
                   <th>Ед.изм.</th>
                   <th>Сумма</th>
                   <th style="width:10px"></th>
                 </tr>
                <?php #unset($_SESSION["prihodList"]);   
                    if(isset($sostavList)){
                    foreach($sostavList as $k=>$v){ 
                ?>
                 <tr id="<?php echo $v["id"]?>" class="sostavlist">
                   <td><?php echo $v["name"] ?></td>
                   <td><?php echo $v["count"] ?></td>
                   <td><?php echo $v["price"] ?></td>
                   <td><?php echo $v["ed_izm_name"] ?></td>
                   <td><?php echo $v["summa"] ?></td>
                   <td><a class="delFromSostavList" id="<?php echo $v["id"] ?>" href="/shop/menu/DelFromSostavList/<?php echo $v["id"] ?>/<?php echo $v["id_product"] ?>"><i class="fa fa-remove">&nbsp;</i></td>
                 </tr>
                <?php }} ?>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
        </form>
                <div class="col-md-2">
            <button type="button" id="<?php echo $id ?>" class="btn btn-block btn-info updateProductPrice">Сохранить</button>
        </div>
           <!-- /.card -->
         </div>
         </div>
          <div class="col-md-3">   
             <form action="/shop/menu/AddToSostavList/<?php echo $id ?>" method="post" id="addToSostavList">
              <div class="card card-info">
                  <div class="card-header">
               <h3 style="display:inline" class="card-title">Ингредиенты</h3>
             </div>
             <div class="card-body ">
                 
                 <div class="form-group">
                     <input type="text" name="shcode" autocomplete="off" id="shcode" class="form-control" id="shcode" placeholder="Штрихкод">
                 </div>
                 <div class="form-group">
                     <input name="name" autocomplete="off" id="name" required="true" minlength="2" class="typeahead form-control">
                     <input name="id" id="id" type="hidden" >
                     
                 </div>
                 <div class="form-group">
                      <select name="ed_izm" id="ed_izm" data-placeholder="Ед.изм." class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                       <option></option>
                       </div><?php echo Utils::getOptionList(Spr::getByCategory(Spr::ed_izm));?>
                     </select>
                     <input name="ed_izm_name" id="ed_izm_name" type="hidden">
                     <input name="price" id="price" type="hidden">
                     <input name="id_product" id="id_product" type="hidden" value="<?php echo $id ?>">
                 </div>
                 <div class="form-group">
                     <input name="count" type="text" class="form-control" required="true" minlength="1" id="count" autocomplete="off" placeholder="Количество">
                 </div>
                 <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
             </div>


           </div> 
            </form>

          </div>
         </div>

     </div><!-- /.container-fluid -->
   </section>
   <!-- Modal -->
<div class="modal fade" id="addNewDish" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Добавление нового блюда</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="addNewDish" name="newDishForm" action="/shop/menu/AddNewDish" method="post">
                <div class="form-group">
                    <input name="name" autocomplete="off" id="product_name" required="true" minlength="2" placeholder="Наименование" class="typeahead form-control">
                    <input name="id" id="id" type="hidden">
                </div> 
                <div class="form-group">
                    <input type="text" name="price_in" autocomplete="off" id="price" class="form-control" placeholder="Цена покупки">
                </div> 
                <div class="form-group">
                    <input type="text" name="price_out" autocomplete="off" id="price" class="form-control" placeholder="Цена продажи">
                </div>
                <div class="form-group">
                    <select name="id_category" id="id_category" placeholder="Категория" class="form-control">
                        <?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_category))?>                    
                    </select>
                </div>
                    
                <div class="form-group">
                    <select name="id_factory" id="id_factory" placeholder="Цех" class="form-control">
                        <?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_factory))?>                    
                    </select>
                </div>
                <div class="form-group">
                    <input autocomplete="off" name="coock_time" type="text" placeholder="Время приготовления" class="form-control" required="true" minlength="1" id="coock_time">
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>


           </div> 
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<?php include ROOT . '/views/layouts/footer.php'; ?>