<title>Касса</title>
<?php include ROOT . '/views/layouts/header.php';?>
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
        <form action="/shop/report/Filter" method="POST">
           <input type="hidden" name="report" value="Payments">
         <div class="row" style="margin-bottom:5px">
             <div class="col-md-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                    <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="d1" value="<?php echo $d1; ?>">
                </div>
             </div>
             
             <div class="col-md-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                    <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="d2" value="<?php echo $d2; ?>">
                </div>
             </div> 
             <div class="col-md-2">
                <div class="input-group">
                    <select name="firma" id="firma" class="select2">
                        <option value="0"></option>
                        <?php echo Utils::getOptionList($firmaArr,$firma)?></select>
                </div>
             </div> 
             <div class="col-md-2">
                <div class="input-group">
                    <select name="worker" id="worker" class="select2">
                        <option value="0"></option>
                        <?php echo Utils::getOptionList($usersArr,$worker,"user_fio")?></select>
                </div>
             </div> 
            <div class="col-md-1">
                <input type="submit" value="Применить" class="btn btn-block btn-info form-control">
            </div>
         
         </form>
         <div class="col-md-1">
            <form action="/shop/report/Filter" method="POST">
              <input type="hidden" name="report" value="Payments">
              <input type="hidden" name="d1" value="<?php echo date("d-m-Y") ?>">
              <input type="hidden" name="d2" value="<?php echo date("d-m-Y") ?>">
              <input type="hidden" name="firma" value="0">
              <input type="hidden" name="worker" value="0">
              <input type="submit" value="Сброс" class="btn btn-block btn-info form-control">
            </form>
            </div> 
         </div>
         <div class="row">
              <div class="col-md-12">
                   <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="saleList">
                 <thead>
                   <tr>
                   <th>ID</th>
                   <th>Дата</th>
                   <th>Сумма прихода</th>
                   <th>Сумма расхода</th>
                   <th>Фирма</th>
                   <th>Сотрудник</th>
                   <th>Назначение</th>
                 </tr>
                 </thead>
                 <tbody>
                <?php #unset($_SESSION["prihodListShow"]);   
                    if(isset($list)){
                        $sum_in = $sum_out = 0;
                    foreach($list as $k=>$v){ 
                        $sum_in += $v["sum_in"];
                        $sum_out += $v["sum_out"];
                ?>
                    <tr>
                    <td><?php echo $v["id"] ?></td>
                    <td><?php echo $v["datetime"] ?></td>
                    <td><?php echo $v["sum_in"] ?></td>
                    <td><?php echo $v["sum_out"] ?></td>
                    <td><?php echo $v["firma_name"] ?></td>
                    <td><?php echo $v["worker_name"] ?></td>
                    <td><?php echo $v["nazn"] ?></td>
                 </tr>
                <?php }} ?>
                 <tr>
                     <td colspan="2">Всего</td>
                     <td><?php echo $sum_in ?></td>
                     <td><?php echo $sum_out ?></td>
                 </tr>
                 </tbody>
               </table>
             </div>
             
               <!-- Modal -->
            <div class="modal fade" id="closeSaleModal" tabindex="-1" role="dialog"  aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                 <form id="closeSaleForm" name="closeSaleForm" action="/shop/kassa/CloseSale" method="post">
                    <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-usd"></i> Оплата</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-info">
                            <div class="card-body">
                                <h2><p style="display:inline" id="stol"></p></h2>
                                <h2><p style="display:inline">Сумма покупки:</p><p style="display:inline" id="saleSum"></p></h2>
                                <h2><p style="display:inline">Оплачено:<input style="width:150px;display:inline;margin-left:5px" autocomplete="off" class="form-control" name="paySum" ></h2>
                                <h2><p style="display:inline">Сдача:</p>  <p style="display:inline" id="sdacha"></p></h2>
                                <select name="worker" id="worker" data-placeholder="Выберите сотрудника" class="form-control" style="width: 100%;">
                                    <option value="0"></option>
                                    <?php echo Utils::getOptionList($users,0,"user_fio")?>                     
                                </select>
                            <!-- /.card-body -->
                                <div class="form-group">
                                    <h3 style="display:inline">Распечатать чек:</h3>
                                    <input style="display:inline" type="checkbox" id="printSale" name="printSale" data-toggle="toggle" data-size="large" data-on="Да" data-off="Нет">
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary" disabled="true" id="pay">Оплатить</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
             <!-- /.card-body -->
           </div>
              </div>
          </div>
     </div><!-- /.container-fluid -->
   </section>
   <style>
       .dataTables_filter{
           height: 44px;
           font-size: 1.2em;
       }
       #worker option{
            padding: 8px;
            font-size: 2em;
       }
   </style>    
<?php include ROOT . '/views/layouts/footer.php'; ?>