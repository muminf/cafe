<title>Закрытые чеки</title>
<?php include ROOT . '/views/layouts/header.php';

?>
<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
        <div clas="row">
                <form action="/shop/report/Filter" method="POST">
                    <div class="col-sm-2" style="display:inline-block">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                            <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="d1" value="<?php echo $d1; ?>">
                        </div>
                    </div>
                    <div class="col-sm-2" style="display:inline-block">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                            <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" name="d2" value="<?php echo $d2; ?>">
                        </div>
                    </div>
                     <div class="col-sm-2" style="display:inline-block">
                        <button type="submit" class="btn btn-block btn-info">Применить</button>
                     </div>
                </form>
         </div>
          <div class="row">
              <div class="col-md-9">
                  <h6 style="display: inline;margin-left:20px">Дата: <?php echo date("d.m.Y")?></h6>
                  <h6 style="display: inline;margin-left:20px">Приход: <?php echo $input?>с</h6>
                  <h6 style="display: inline;margin-left:20px">Расход: <?php echo $output?>с</h6>
                  <h6 style="display: inline;margin-left:20px">Остаток: <?php echo $saldo?>с</h6>
              </div>  
          </div>
        
                  <div class="row">
              <div class="col-md-12">
                   <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="saleList">
                 <thead>
                   <tr>
                   <th>Номер</th>
                   <th>Стол/Клиент</th>
                   <th>Официант</th>
                   <th>Сумма</th>
                   <th>Открыт</th>
                   <th></th>
                 </tr>
                 </thead>
                 <tbody>
                <?php #unset($_SESSION["prihodListShow"]);   
                    if(isset($list)){
                    foreach($list as $k=>$v){ 
                        $v["sum_out"] = (is_null($v["sum_out"]) ? 0 : $v["sum_out"]);
                ?>
                    <tr class="list <?php echo ($v["id"] == $id ? "selected" : "")?>">
                    <td><?php echo $v["id"] ?></td>
                    <td><?php echo $v["stol"].$v["client"] ?></td>
                    <td><?php echo $v["oficiant"] ?></td>
                    <td><?php echo $v["all_sum"] ?></td>
                    <td><?php echo $v["beg_time"] ?></td>
                    <td><a class="btn btn-info" id="<?php echo $v["id"] ?>" href="/shop/report/PrintCash/<?php echo $v["id"] ?>/<?php echo $d1;?>/<?php echo $d2;?>" role="button">Чек</a></td>
                 </tr>
                <?php }} ?>
                 </tbody>
               </table>
             </div>
             
             <!-- /.card-body -->
           </div>
              </div>
          </div>
          <div class="row"> 
            <div class="col-md-12">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Список заказов</h3>
               <h3 style="display:block;float:right;" class="card-title"></h3>
             </div>

          
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="saleInfo">
                 <tr>
                   <th>Наименование</th>
                   <th>Количество</th>
                   <th>Цена</th>
                   <th>Сумма</th>
                 </tr>
                <?php 
                    foreach($info as $k=>$v){ 
                ?>
                 <tr>
                   <td><?php echo $v["product"] ?></td>
                   <td><?php echo $v["count"] ?></td>
                   <td><?php echo $v["price_out"] ?></td>
                   <td><?php echo $v["count"]*$v["price_out"] ?></td>
                 </tr>
                <?php } ?>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->
         </div>
         </div>
        
         </div>
          <!-- Modal -->
<div class="modal fade" id="addNewRashod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Добавление нового блюда</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="newRashodForm" name="newRashodForm" action="/shop/kassa/AddNewRashod" method="post">
                <div class="form-group">
                    <input name="sum_out" autocomplete="off" id="sum_out" required="true" minlength="1" placeholder="Сумма" class="form-control">
                </div>
                <div class="form-group">
                    <select name="id_agent" id="id_agent" placeholder="Агент" class="form-control select2">
                        <?php echo Utils::getOptionList(Utils::getList(Agent::tbl_name,"id,name","status=".Spr::status_active))?>                    
                    </select>
                </div>
                <div class="form-group">
                    <textarea name="nazn" placeholder="Назначение платежа" class="form-control" id="nazn"></textarea>
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>


           </div> 
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
     </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>