<title>Накладные</title>
<?php include ROOT . '/views/layouts/header.php'; 
if(!isset($_SESSION["prihodListShow"])){
    $_SESSION["prihodListShow"] = array();
}
#unset($_SESSION["prihodListShow"]);
#Utils::pre($_SESSION["prihodListShow"]);
$ids = $_SESSION["prihodListShow"];
$list_ext = Prihod::getItemListByIds($ids);
#Utils::pre($list_ext);
?>

<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-4">
            <h1>Накладные</h1>
        </div>
        <div class="row">
            <form action="/shop/sklad/filter" method="POST">
                <div class="col-sm-8">
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="d1" value="<?php echo $d1; ?>">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" name="d2" value="<?php echo $d2; ?>">
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-info">Применить</button>
            </form>
        </div>
        <div class="col-sm-5">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/sklad/new">Приход</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/list/<?php echo date("d-m-Y");?>/<?php echo date("d-m-Y");?>">Накладные</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/saldo">Остаток</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/spisat">Списание</a></li>
            </ol>
        </div>
            
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                   <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="prihodList">
                 <thead>
                   <tr>
                   <th>Номер</th>
                   <th>Время прихода</th>
                   <th>Пользователь</th>
                   <th>Сумма</th>
                 </tr>
                 </thead>
                 <tbody>
                <?php #unset($_SESSION["prihodListShow"]);   
                    if(isset($list)){
                    foreach($list as $k=>$v){ 
                ?>
                    <tr class="list <?php echo (array_key_exists($v["id"], $_SESSION["prihodListShow"]) ? "selected" : "")?>">
                    <td><?php echo $v["id"] ?></td>
                    <td><?php echo $v["datetime"] ?></td>
                    <td><?php echo $v["user"] ?></td>
                    <td><?php echo $v["summa"] ?></td>
                 </tr>
                <?php }} ?>
                 </tbody>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
              </div>
          </div>
          <div class="row"> 
            <div class="col-md-12">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Список товаров</h3>
               <h3 style="display:block;float:right;" class="card-title"></h3>
             </div>

          
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="prihodListInfo">
                 <tr>
                   <th>Штрихкод</th>
                   <th>Наименование</th>
                   <th>Ед.изм.</th>
                   <th>Поставщик</th>
                   <th>Цена</th>
                   <th>Количество</th>
                   <th>Сумма</th>
                 </tr>
                <?php #unset($_SESSION["prihodList"]);   
                    if(isset($list_ext)){
                    foreach($list_ext as $k=>$v){ 
                ?>
                 <tr id="r<?php echo $v["id"]?>" class="list">
                   <td><?php echo $v["shcode"] ?></td>
                   <td><?php echo $v["name"] ?></td>
                   <td><?php echo $v["ed_izm_name"] ?></td>
                   <td><?php echo $v["postavshik"] ?></td>
                   <td><?php echo $v["price"] ?></td>
                   <td><?php echo $v["count"] ?></td>
                   <td><?php echo $v["count"]*$v["price"] ?></td>
                 </tr>
                <?php }} ?>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->
         </div>
         </div>
        
         </div>
      
     </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>