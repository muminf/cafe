<title>Списание материалов</title>
<?php include ROOT . '/views/layouts/header.php'; 

?>

<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Списать блюда</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/sklad/new">Приход</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/list/<?php echo date("d-m-Y");?>/<?php echo date("d-m-Y");?>">Накладные</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/saldo">Остаток</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/spisat">Списание</a></li>
            </ol>
        </div>
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row">
              <div class="col-md-12" style="height:100%">
                   <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <form action="/shop/sklad/SpisatExec" method="POST">
               <table class="table table-hover" id="prihodList">
                 <thead>
                   <tr>
                    <th>Наименование</th>
                    <th>Остаток</th>
                    <th>Цена</th>
                    <th>Сумма</th>
                    <th>Списать</th>
                 </tr>
                 </thead>
                 <tbody>
                <?php #unset($_SESSION["prihodListShow"]);   
                    if(is_array($list)){
                    foreach($list as $k=>$v){ 
                ?>
                <tr class="list">
                    <td><?php echo $v["name"] ?></td>
                    <td><?php echo $v["saldo"] ?></td>
                    <td><?php echo $v["price"] ?></td>
                    <td><?php echo $v["price"]*$v["saldo"] ?></td>
                    <td><input type="text" name="<?php echo $v["id"] ?>"></td>
                </tr>
                <?php }} ?>
                 </tbody>
               </table>
                   <input type="submit">
               </form>
             </div>
             <!-- /.card-body -->
           </div>
              </div>
          </div>
     </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>