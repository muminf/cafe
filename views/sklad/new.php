<title>Приход</title>
<?php include ROOT . '/views/layouts/header.php'; 
   #Utils::pre(Spr::getByCategory(1));
   #Utils::pre(Utils::getList(Agent::tbl_name,"id,name","id_type=".Agent::postavshik));
#Utils::pre($_SESSION["prihodList"]);
#echo Utils::getLastId(Material::tbl_name);
if(!isset($_SESSION["prihodList"])){
    $_SESSION["prihodList"] = array();
}
?>

<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-2">
            <h1>Приход</h1>
        </div>
        <div class="col-sm-2">
            <button type="button" id="savePrihod" class="btn btn-block btn-info">Сохранить</button>
        </div>
        <div class="col-md-3">
         <!-- Button trigger modal -->
         <div class="input-group mb-4">
             <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#addNewProduct">
                 <span style="float:left"><i class="fa fa-plus">&nbsp;</i></span> Добавить новый продукт
             </button>
         </div>
        </div>
        <div class="col-sm-5">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/sklad/new">Приход</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/list/<?php echo date("d-m-Y");?>/<?php echo date("d-m-Y");?>">Накладные</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/saldo">Остаток</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/spisat">Списание</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/CleanPrihodList">Очистить список</a></li>
            </ol>
        </div>
            
       </div>
         
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row"> 
            <div class="col-md-9">
            <div class="card card-info">
             <div class="card-header">
               <h3 style="display:inline" class="card-title">Приход</h3>
               <h3 style="display:block;float:right;" class="card-title">Всего на сумму: <div id="allSum" style="display:inline"><?php echo Prihod::getItemSumPrihod($_SESSION["prihodList"])?></div></h3>
             </div>

          
           <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="prihodListTable">
                 <tr>
                   <th>Штрихкод</th>
                   <th>Наименование</th>
                   <th>Поставщик</th>
                   <th>Цена</th>
                   <th>Количество</th>
                   <th>Ед.изм.</th>
                   <th>Сумма</th>
                   <th style="width:10px"></th>
                 </tr>
                <?php #unset($_SESSION["prihodList"]);   
                    if(isset($_SESSION["prihodList"])){
                    $list = array_reverse($_SESSION["prihodList"]); 
                    foreach($list as $k=>$v){ 
                ?>
                 <tr id="r<?php echo $v["id"]?>" class="list">
                   <td><?php echo $v["shcode"] ?></td>
                   <td><?php echo $v["name"] ?></td>
                   <td><?php echo $v["postavshik"] ?></td>
                   <td><?php echo $v["price"] ?></td>
                   <td><?php echo $v["count"] ?></td>
                   <td><?php echo $v["ed_izm_name"] ?></td>
                   <td><?php echo $v["count"]*$v["price"] ?></td>
                   <td><a class="prihodItem" id="<?php echo $v["id"]?>" href="/shop/sklad/DelFromPrihodList/<?php echo $v["id"]?>"><i class="nav-icon fa fa-remove"></i></a></td>
                 </tr>
                <?php }} ?>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->
         </div>
         </div>
          <div class="col-md-3">   
             <div class="card card-info">
             <div class="card-body ">
                 <form action="/shop/sklad/addToPrihodList" method="post" id="addToPrihodList">
                 <div class="form-group">
                     <input type="text" name="shcode" autocomplete="off" id="shcode" class="form-control" id="shcode" placeholder="Штрихкод">
                 </div>
                 <div class="form-group">
                     <input name="name" autocomplete="off" id="name" required="true" minlength="2" maxlength="20" class="typeahead form-control">
                     <input name="id" id="id" type="hidden">
                     <input name="id_product" id="id_product" type="hidden">
                 </div>
                 <div class="form-group">
                      <select name="id_postavshik" id="id_postavshik" data-placeholder="Поставщик" class="form-control" style="width: 100%;">
                       <?php echo Utils::getOptionList(Utils::getList(Agent::tbl_name,"id,name"));?>
                     </select>
                     <input name="postavshik" id="postavshik" type="hidden">
                 </div>
                 <div class="form-group">
                      <select name="ed_izm" id="ed_izm" data-placeholder="Ед.изм." class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true">
                       </div><?php echo Utils::getOptionList(Spr::getByCategory(Spr::ed_izm),2);?>
                     </select>
                     <input name="ed_izm_name" id="ed_izm_name" type="hidden">
                 </div>
                 <div class="form-group">
                     <input name="count" type="text" autocomplete="off" class="form-control" required="true" minlength="1" id="count" placeholder="Количество">
                 </div>
                 <div class="form-group">
                     <input name="price" id="price" type="text" class="form-control" required="true" minlength="1" placeholder="Цена">
                 </div>
                 <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
             </div>
            </form>
           </div> 
          </div>
         </div>
       <!-- Modal -->
<div class="modal fade" id="addNewProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-plus"></i> Добавление нового продукта</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="card card-info">
            <div class="card-body">
                <form id="newProductForm" name="newProductForm" action="/shop/sklad/AddNewProduct" method="post">
                <div class="form-group">
                    <input name="product_shcode" autocomplete="off" id="product_shcode"  minlength="8" placeholder="Штрихкод" class="form-control">
                </div>
                    <div class="form-group">
                    <input name="product_name" autocomplete="off" id="product_name" required="true" minlength="2" minlength="20" placeholder="Наименование" class="typeahead1 form-control">
                </div>
                <div class="form-group">
                    <select name="ed_izm" id="ed_izm" placeholder="Ед.изм." class="form-control">
                        <?php echo Utils::getOptionList(Spr::getByCategory(Spr::ed_izm),1)?>                    
                    </select>
                </div>
             <!--   <div class="form-group">
                    <input type="number" name="count" autocomplete="off" id="count" class="form-control" placeholder="Количество">
                </div> -->
                <div class="form-group">
                    <input name="price_in" autocomplete="off" id="price" class="form-control" placeholder="Цена покупки">
                </div> 
                <div class="form-group">
                    <input name="price_out" autocomplete="off" id="price" class="form-control" placeholder="Цена продажи">
                </div>
                <div class="form-group">
                    <select name="id_postavshik" id="id_postavshik" placeholder="Поставщик" class="form-control">
                        <?php echo Utils::getOptionList(Utils::getList(Agent::tbl_name,"id,name","id_type=".Agent::postavshik));?>                    
                    </select>
                </div>
                <div class="form-group">
                    <select name="id_category" id="id_category" placeholder="Категория" class="form-control">
                        <?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_category),19)?>                    
                    </select>
                </div>
                    
                <div class="form-group">
                    <select name="id_factory" id="id_factory" placeholder="Цех" class="form-control">
                        <?php echo Utils::getOptionList(Spr::getByCategory(Spr::menu_factory),7)?>                    
                    </select>
                </div>
                <div class="form-group">
                    <input autocomplete="off" name="coock_time" type="text" placeholder="Время приготовления" class="form-control" required="true" minlength="1" id="coock_time">
                </div>
                <button type="submit" class="btn btn-block btn-info">Добавить</button>
             <!-- /.card-body -->
            </form>
         </div>


           </div> 
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary">Сохранить</button>
      </div>
    </div>
  </div>
</div>
     </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>