<title>Склад - остаток</title>
<?php include ROOT . '/views/layouts/header.php'; ?>

<!-- Content Header (Page header) -->
   <section class="content-header">
     <div class="container-fluid">
       <div class="row mb-2">
        <div class="col-sm-7">
            <h1>Остаток товаров на складе</h1>
        </div>
      <!--  <div class="row">
            <form action="/shop/sklad/filter" method="POST">
                <div class="col-sm-8">
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="d1" value="<?php echo $d1; ?>">
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input autocomplete="off" type="text" class="form-control datemask" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" name="d2" value="<?php echo $d2; ?>">
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-info">Применить</button>
            </form>
        </div> -->
        <div class="col-sm-5">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/shop/sklad/new">Приход</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/list/<?php echo date("d-m-Y");?>/<?php echo date("d-m-Y");?>">Накладные</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/saldo">Остаток</a></li>
                <li class="breadcrumb-item"><a href="/shop/sklad/spisat">Списание</a></li>
            </ol>
        </div>
       </div>
     </div><!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
     <div class="container-fluid">
          <div class="row">
              <div class="col-md-12" style="height:100%">
                   <div class="card">
             <!-- /.card-header -->
             <div class="card-body table-responsive p-0">
               <table class="table table-hover" id="skladSaldoList">
                 <thead>
                   <tr>
                   <th>ID</th>
                   <th>Поставщик</th>
                   <th>Наименование</th>
                   <th>Остаток</th>
                   <th>Цена</th>
                   <th>Сумма</th>
                   <th>Последний приход</th>
                 </tr>
                 </thead>
                 <tbody>
                <?php #unset($_SESSION["prihodListShow"]);   
                    if(isset($list)){
                    foreach($list as $k=>$v){ 
                ?>
                <tr class="list">
                    <td><?php echo $v["id"] ?></td>
                    <td><?php echo $v["postavshik"] ?></td>
                    <td><?php echo $v["name"] ?></td>
                    <td><?php echo $v["saldo"] ?></td>
                    <td><?php echo $v["price"] ?></td>
                    <td><?php echo $v["price"]*$v["saldo"] ?></td>
                    <td><?php echo $v["last_input"] ?></td>
                </tr>
                <?php }} ?>
                 </tbody>
               </table>
             </div>
             <!-- /.card-body -->
           </div>
              </div>
          </div>
     </div><!-- /.container-fluid -->
   </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>