<?php
User::checkAccess();
class SprController
{
	public function actionIndex()
	{
            if(!isset($_SESSION["spr"]["id"])){
                $_SESSION["spr"]["id"] = 0;
            }
            $id = ($_SESSION["spr"]["id"]>0 ? $_SESSION["spr"]["id"] : 0);
            $list = Spr::getByCategory($id);
            require_once ROOT.'/views/spr/index.php';
            return true;
	}
        
	public function actionStoliki()
	{
            if(!isset($_SESSION["spr"]["id"])){
                $_SESSION["spr"]["id"] = 0;
            }
            $id = ($_SESSION["spr"]["id"]>0 ? $_SESSION["spr"]["id"] : 0);
            $list = Utils::getList("stoliki");
            require_once ROOT.'/views/spr/stoliki.php';
            return true;
	}
        
        public function actionAgents()
	{
            $list = Utils::getList(Agent::tbl_name);
            require_once ROOT.'/views/spr/agents.php';
            return true;
	}
        
        public function actionUsers()
	{
            $list = Utils::getList(User::tbl_name);
            require_once ROOT.'/views/spr/users.php';
            return true;
	}
        
        public function actionEditSpr($id)
        {
            $_SESSION["spr"]["id"] = $id;
            
            $list = Spr::getByCategory($id);
            echo json_encode($list);
            return true;
        }
        
        public function actionEditSprExec()
        {
            $update = array();
            foreach($_POST as $k=>$v){
                if(strlen($v)>0){
                    $update[$k]["id"] = $k;
                    $update[$k]["name"] = substr($v,0,40);
                }
            }
            Utils::update(Spr::spr_val,$update);
            header("Location:/shop/spr");
            return true;
        }
        
        public function actionAddNewSprVal()
        {
            $id = $_SESSION["spr"]["id"];
            if($id>0){
                $insert = array("id_spr"=>$id,"name"=>substr($_POST["name"],0,40));
                Utils::insert(Spr::spr_val,$insert);
            }
            header("Location:/shop/spr");
            return true;
        }
        
        public function actionEditStolikiExec()
        { 
            $update = array();
            foreach($_POST["name"] as $k=>$v){
                if(strlen($v)>0){
                    $update[$k]["id"] = $k;
                    $update[$k]["name"] = substr($v,0,40);
                    $update[$k]["chair_count"] = intval($_POST["chair_count"][$k]);
                    $update[$k]["id_type"] = intval($_POST["id_type"][$k]);
                }
            }
            Utils::update("stoliki",$update);
            header("Location:/shop/spr/Stoliki");
            return true;
        }
        
        public function actionEditAgentExec()
        { 
            $update = array();
            foreach($_POST["name"] as $k=>$v){
                if(strlen($v)>0){
                    $update[$k]["id"] = $k;
                    $update[$k]["name"] = substr($v,0,40);
                    $update[$k]["phone"] = substr($_POST["phone"][$k],0,20);
                }
            }
            Utils::update(Agent::tbl_name,$update);
            header("Location:/shop/spr/Agents");
            return true;
        }
        
        public function actionEditUserExec()
        { 
            $update = array();
            foreach($_POST["name"] as $k=>$v){
                if(strlen($v)>0){
                    $update[$k]["id"] = $k;
                    $update[$k]["name"] = substr($v,0,40);
                    $update[$k]["login"] = substr($_POST["login"][$k],0,10);
                    $update[$k]["phone"] = substr($_POST["phone"][$k],0,20);
                    $update[$k]["status"] = intval($_POST["status"][$k]);
                    $update[$k]["type"] = intval($_POST["type"][$k]);
                }
            }
            Utils::update(User::tbl_name,$update);
            header("Location:/shop/spr/Users");
            return true;
        }
        
        public function actionAddNewStolik(){
            Utils::insert("stoliki",$_POST);
            header("Location:/shop/spr/Stoliki");
            return true;
        }
        public function actionAddNewAgent(){
            if(intval($_POST["id"])<1){
                unset($_POST["id"]);
                Utils::insert(Agent::tbl_name,substr($_POST,0,40));
            }
            header("Location:/shop/spr/Agents");
        }
        
        public function actionAddNewUser(){
            $user = Utils::getList(User::tbl_name,"id","login='".substr($_POST["login"],0,10)."'",1);
            if(intval($_POST["id"])<1 && !is_array($user)){
                unset($_POST["id"]);
                $_POST["name"] = substr($_POST["name"],0,40);
                $_POST["login"] = substr($_POST["login"],0,10);
                $_POST["password"] = User::getPasswordHash($_POST["password"]);
                $_POST["phone"] = substr($_POST["phone"],0,20);
                #Utils::pre($_POST);
                Utils::insert(User::tbl_name,$_POST);
            }
            header("Location:/shop/spr/Users");
        }
}
?>