<?php
User::checkAccess();
class MenuController
{
	public function actionIndex()
	{
            require_once ROOT.'/views/menu/index.php';
            return true;
	}
        
        public function actionMenu()
        {
            $list = Product::getList();
            require_once ROOT.'/views/menu/menu.php';
            return true;
        }
        
        public function actionSostav()
	{
            if(!isset($_SESSION["product"]["id"])){
                $_SESSION["product"]["id"] = 0;
            }
            $id = ($_SESSION["product"]["id"]>0 ? $_SESSION["product"]["id"] : 0);
            $sostavList = Product::getSostav($id);
            $allSum = 0;
            if(is_array($sostavList)){
                foreach($sostavList as $k=>$v){
                    $allSum += $v["summa"];
                }
            }
            $allSum = round($allSum,2);
            require_once ROOT.'/views/menu/sostav.php';
            return true;
	}
        
        public function actionSaldo()
	{
            $list = Utils::getList(Product::tbl_name);
            require_once ROOT.'/views/menu/saldo.php';
            return true;
	}
        
        public function actionUpdateMenuSaldo()
	{
            $update = array();
            $insert = array();
            $ids = array();
            foreach($_POST as $k=>$v){
                $v = floatval($v);
                if($v>0){
                    $update[$k] = array("id" => $k,"saldo+" => $v);
                    $insert[$k] = array("id_product"=>intval($k),
                                        "id_user"=>$_SESSION["user"]["id"],
                                        "count" => $v,
                                        "datetime"=>date("Y-m-d H:i:s"));
                    $ids[$k] = $v;
                }
            }
            unset($_POST);
            if(count($update)>0){
                Utils::update(Product::tbl_name, $update);
                Utils::insert(Product::tbl_ready, $insert);
                $list = Utils::getList(Product::tbl_sostav, null, " id_product in(". implode(",", array_keys($ids)).")");
                $minus = array();
                if(is_array($list)){
                    foreach($list as $k=>$v){
                        if(array_key_exists($v["id_product"], $ids)){
                            if(isset($minus[$v["id_material"]])){
                                $minus[$v["id_material"]]["saldo-"] += $v["count"]*$ids[$v["id_product"]];
                            }else{
                                $minus[$v["id_material"]] = array("id" => $v["id_material"],
                                                              "saldo-" => $v["count"]*$ids[$v["id_product"]]);
                            }
                        }
                    }
                    Utils::update(Material::tbl_name, $minus);
                }
            }
            
            header("Location:/shop/menu/saldo");
            return true;
	}
        
        public function actionSpisat()
	{
            $list = Utils::getList(Product::tbl_name,null,"saldo>0");
            require_once ROOT.'/views/menu/spisat.php';
            return true;
	}
        
        public function actionSpisatExec()
	{
            $update = array();
            $insert = array();
            if(isset($_POST)){
                foreach($_POST as $k=>$v){
                    $v = floatval($v);
                    $k = intval($k);
                    if($v>0){
                        $update[$k] = array("id" => $k,"saldo-" => $v);
                        $insert[$k] = array("id_product"=>$k,
                                        "id_user"=>$_SESSION["user"]["id"],
                                        "count" => $v,
                                        "datetime"=>date("Y-m-d H:i:s"));
                    }
                }
                if(count($update)>0){
                    Utils::update(Product::tbl_name, $update);
                    Utils::insert(Product::tbl_spisan, $insert);
                }
                unset($_POST);
            }
            header("Location:/shop/menu/spisat");
            return true;
        }
        public function actionEditProduct($id)
        {
            $_SESSION["product"]["id"] = $id;
            $id = ($_SESSION["product"]["id"]>0 ? $_SESSION["product"]["id"] : 0);
            $sostavList = Product::getSostav($id);
            echo json_encode($sostavList);
            return true;
        }
        
        public function actionAddToSostavList()
        {
            $_POST["id_product"] = $_SESSION["product"]["id"];
            $_POST["count"] = str_replace(",", ".", $_POST["count"]);
            $item = Utils::getList(Product::tbl_sostav, "id","id_product=".intval($_POST["id_product"])." and id_material=".intval($_POST["id"])." limit 1");
            $id_sostav = (is_array($item) ? key($item) : 0);
            if($id_sostav>0){
                $update = array();
                $update["id"] = $id_sostav;
                $update["count"] = floatval($_POST["count"]);
                Utils::update(Product::tbl_sostav, $update);
                $list = Product::getSostav(intval($_POST["id_product"]),$id_sostav);
                echo json_encode($list);
            }else{
                if($new = Product::addToSostav($_POST)){
                    $list = Product::getSostav(intval($_POST["id_product"]),$new);
                    echo json_encode($list);
                }
            }
            Product::UpdateOrigPrice(intval($_POST["id_product"]));
            header("Location:/shop/menu/sostav");
            return true;
        }
        
        public function actionDelFromSostavList($id,$id_product)
        {
            if(Product::delFromSostav($id)){
                echo 1;
            }
            Product::UpdateOrigPrice($id_product);
            header("Location:/shop/menu/sostav");
            return true;
            
        }
        
        public function actionGetByName($name){
            $res = Product::getByName(substr($name,0,60));
            echo json_encode($res);
            return true;
        }
        
        public function actionAddNewDish()
        {
            $new = array();
            $new["name"] = substr($_POST["name"],0,60);
            $new["price_in"] = str_replace(",", ".", $_POST["price_in"]);
            $new["price_out"] = str_replace(",", ".",$_POST["price_out"]);
            $new["price_in"] = floatval($new["price_in"]);
            $new["price_out"] = floatval($new["price_out"]);
            $new["income"] = $_POST["price_out"]-$_POST["price_in"];
            $new["id_category"] = intval($_POST["id_category"]);
            $new["id_factory"] = intval($_POST["id_factory"]);
           # $new["coock_time"] = $_POST["coock_time"];
            $id = Utils::insert(Product::tbl_name, $new);
            $res = array("id"=>$id,"name"=>$new["name"]);
            echo json_encode($res);
            return true;
        }
        
        public function actionUpdateProductPrice($id_product)
        {
            echo $id_product;
            Product::updateOrigPrice($id_product);
            return true;
        } 
        public function actionSetEditProductId($id){
            $_SESSION["edit_product"] = intval($id);
            $info = Utils::getList(Product::tbl_name, null, "id=".intval($id));
            echo json_encode($info[$id]);
            return true;
        } 
        public function actionEditProductItem($id){
            $_POST["id"] = $id;
            $_POST["name"] = substr($_POST["name"],0,60);
            $_POST["id_category"] = intval($_POST["id_category"]);
            $_POST["id_factory"] = intval($_POST["id_factory"]);
            $_POST["price_out"] = str_replace(",", ".", $_POST["price_out"]);
            $_POST["price_out"] = floatval($_POST["price_out"]);
            $_POST["status"] = intval($_POST["status"]);
            Utils::update(Product::tbl_name, $_POST);
            header("Location:/shop/menu/menu");
            return true;
        }
        
}
?>