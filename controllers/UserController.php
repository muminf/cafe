<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author M_Fayziev
 */
class UserController {
    const tbl_name = 'users';
    public function actionRegister()
    {
        $name = '';
        $login = '';
        $password = '';
        $result = false;
        if(isset($_POST["submit"])){
            $name = $_POST["name"];
            $login = $_POST["login"];
            $password = $_POST["password"];
            
            $errors = false;
            
            if(!User::checkName($name)){
                $errors[] = 'Имя должно быть не короче 2-х символов';
            }
            
            if(!User::checkLogin($login)){
                $errors[] = 'Неправильный логин';
            }
            /*
            if(!User::checkPassword($password)){
                $errors[] = 'Пароль должен быть не короче 6 символов';
            }
            */
            if(!User::checkLoginExists($login)){
                $errors[] = 'Такой логин уже используется';
            }
            
            if($errors == false){
                $result = User::register($name,$login,$password);
            }
        }
        require_once '/views/user/register.php';
        return true;
    }
    public function actionLogin()
    {
        $login = '';
        $password = '';
        
        if(isset($_POST['login'])){
            $login = substr($_POST['login'],0,10);
            $password = $_POST['password'];
            $errors = false;
            if(!User::checkLogin($login)){
                $errors[] = 'Неправильный логин';
            }
            
            if(!User::checkPassword($password)){
                $errors[] = 'Пароль должен быть не короче 6 символов';
            }
            $userId = User::checkUserData($login,$password);
            if($userId == false){
                $errors[] = 'Неправильные данные для входа на сайт';
            }else{
                $usr = User::auth($userId);
                if($_SESSION['user']["user_type"] == User::admin){
                    header("Location:/shop");
                }else{
                    User::checkAccess();
                }
            }
        }
        require_once ROOT.'/views/user/login.php';
        return true;
    }
    public function actionLogout()
    {
        unset($_SESSION['user']);
        header('Location:/shop/');
    }
    
    public function actionChangePassword()
    {
        if(isset($_POST["password"]) && $_POST["password"] == $_POST["password2"]){
            $password = substr($_POST["password"], 0, 10);
           # $password = User::getPasswordHash($password);
            $id = intval($_SESSION["user"]["user_id"]);
            Utils::update(User::tbl_name, array("user_password"=>$password,"user_id"=>$id),"user_id");
            header("Location:/shop/user/logout");
        }
        $login = User::getUserInfoById($_SESSION["user"]["user_id"]);
        $login = $login["user_login"];
        require_once ROOT.'/views/user/password.php';
        return true;
    }
}
    
