<?php
User::checkAccess();
class SkladController
{
        
	public function actionIndex()
	{
            require_once ROOT.'/views/sklad/index.php';
            User::checkAccess();
            return true;
	}
        
        public function actionPrihod()
	{
            require_once ROOT.'/views/sklad/prihod.php';
            return true;
	}
        
        public function actionSaldo()
	{
            $list = Material::getSaldo();
            
            require_once ROOT.'/views/sklad/saldo.php';
            return true;
	}
        
        public function actionNew()
	{
            require_once ROOT.'/views/sklad/new.php';
            return true;
	}
        
        public function actionFilter()
        {
            $d1 = $_SESSION["d1"] = $_POST["d1"];
            $d2 = $_SESSION["d2"] = $_POST["d2"];
            if(isset($_POST["d1"])){
                header("Location:/shop/sklad/list/".$d1."/".$d2);
            }
        }
        public function actionList($d1,$d2)
	{
            $d1 = (empty($_SESSION["d1"]) ? date("d-m-Y") : $_SESSION["d1"]);
            $d2 = (empty($_SESSION["d2"]) ? date("d-m-Y") : $_SESSION["d2"]);
            $d1 = substr($d1,0,10);
            $d2 = substr($d2,0,10);
            $list = Prihod::getList($d1,$d2);
            require_once ROOT.'/views/sklad/list.php';
            return true;
	}
        
        public function actionGetByShCode($shcode){
            $shcode = substr($shcode,0,13);
            $res = Material::getByShCode($shcode); 
            echo json_encode($res);
            return true;
        }
        
        public function actionGetByName($name){
            $name = substr($name,0,40);
            $res = Material::getByName($name);
            echo json_encode($res);
            return true;
        }
        
        public function actionAddToPrihodList()
        {
            $_POST["count"] = str_replace(",", ".", $_POST["count"]);
            $_POST["price"] = str_replace(",", ".", $_POST["price"]);
            
            $_POST["count"] = floatval($_POST["count"]);
            $_POST["price"] = floatval($_POST["price"]);
            
            if(intval($_POST["id"])<1){
                $newMaterial["shcode"] = (strlen($_POST["shcode"])>0 ? $_POST["shcode"] : Utils::genShcode());
                $newMaterial["shcode"] = substr($newMaterial["shcode"],0,13);
                $newMaterial["name"] = substr($_POST["name"],0,40);
                $newMaterial["id_postavshik"] = intval($_POST["id_postavshik"]);
                $newMaterial["ed_izm"] = intval($_POST["ed_izm"]);
                $newMaterial["price"] = $_POST["price"];
                $newMaterial["saldo"] = $_POST["count"];
                $id = Utils::insert(Material::tbl_name, $newMaterial);
                $id = ($id>0 ? $id : Utils::getLastId(Material::tbl_name));
                $item = Material::getById($id);
                #Utils::pre($item);
                $item["count"] = $_POST["count"];
            }else{
                $item = $_POST;
            }
            $_SESSION["prihodList"][$item["id"]] = $item;
            //echo json_encode($item);
            header("Location:/shop/sklad/new");
            return true;
        }
        public function actionDelFromPrihodList($id)
        {
            unset($_SESSION["prihodList"][$id]);
            header("Location:/shop/sklad/new");
        }
        public function actionSavePrihod()
        {
            //Готовим массив итога прихода и добавляем его в БД
            $newPrihod = array();
            $newPrihod["datetime"] = date("Y-m-d H:i:s");
            $newPrihod["id_user"] = $_SESSION["user"]["id"];
            $newPrihod["summa"] = Prihod::getItemSum($_SESSION["prihodList"]);
            $id = Utils::insert(Prihod::tbl_name, $newPrihod);
            
            //Готовим массив списка прихода и добавляем его в БД
            $id = ($id>0 ? $id : Utils::getLastId(Prihod::tbl_list));
            $list = array();
            $dolg = array();
            $material = array();
            $product = array();
            foreach($_SESSION["prihodList"] as $k=>$v){
                if($v["id_product"]>0){
                    $product[$k]["id"] = $v["id_product"];
                    $product[$k]["saldo+"] = $v["count"];
                }    

                $material[$k]["id"] = $v["id"];
                $material[$k]["price"] = $v["price"];
                $material[$k]["id_postavshik"] = $v["id_postavshik"];
                $material[$k]["ed_izm"] = $v["ed_izm"];
                $material[$k]["last_input"] = date("Y-m-d H:i:s");
                $material[$k]["saldo+"] = $v["count"];

                $list[$k]["id_material"] = $v["id"];
                $list[$k]["price"] = $v["price"];
                $list[$k]["count"] = $v["count"];
                $list[$k]["summa"] = $v["price"]*$v["count"];
                $list[$k]["id_postavshik"] = $v["id_postavshik"];
                $list[$k]["id_prihod"] = $id;
                
                
                if(array_key_exists($v["id_postavshik"],$dolg)){
                    $dolg[$v["id_postavshik"]] += $list[$k]["summa"];
                }else{
                    $dolg[$v["id_postavshik"]] = $list[$k]["summa"];
                }
            }
            if(count($product)>0){
                Utils::update(Product::tbl_name, $product);
            }
            
           # Utils::pre($product);exit;
            echo Utils::insert(Prihod::tbl_list,$list);
            
            //Обновляем задолжености перед поставщиками
            Utils::pre($dolg);
            Agent::updateSaldo($dolg);
            
            //Обновляем свойства материалов
            Utils::update(Material::tbl_name, $material);
            
            //Очищаем массив прихода
            unset($_SESSION["prihodList"]);
            return true;
        }
        public function actionAddToPrihodItog($id)
        {
            $_SESSION["prihodListShow"][$id] = $id;
            Sklad::showPrihodList();
            return true;
        }
        public function actionDelFromPrihodItog($id)
        {
            unset($_SESSION["prihodListShow"][$id]);
            Sklad::showPrihodList();
            return true;
        }
        public  function actionAddNewProduct()
        {
           # Utils::pre($_POST);
            $product = array();
            $_POST["product_name"] = substr($_POST["product_name"],0,40);
            $product["name"] = $_POST["product_name"];
            #$product["saldo"] = $_POST["count"];
            $_POST["price_in"] = str_replace(",", ".",$_POST["price_in"]);
            $_POST["price_out"] = str_replace(",", ".",$_POST["price_out"]);
            
            $_POST["price_in"] = floatval($_POST["price_in"]);
            $_POST["price_out"] = floatval($_POST["price_out"]);
            
            $product["price_in"] = $_POST["price_in"];
            $product["price_out"] = $_POST["price_out"];
            
            $product["price_in"] = $_POST["price_in"];
            $product["price_out"] = $_POST["price_out"];
            
            $product["income"] = $_POST["price_out"]-$_POST["price_in"];
            $product["id_category"] = intval($_POST["id_category"]);
            $product["id_factory"] = intval($_POST["id_factory"]);
            $id_product = Utils::insert(Product::tbl_name, $product);
            
            $material = array();
            $material["shcode"] = (strlen($_POST["product_shcode"])>6 ? $_POST["product_shcode"] : Utils::genShcode());
            
            $material["shcode"] = substr($material["shcode"],0,13);
            
            $material["name"] = $_POST["product_name"];
            $material["ed_izm"] = intval($_POST["ed_izm"]);
            #$material["saldo"] = $_POST["count"];
            $material["price"] = $_POST["price_in"];
            $material["id_postavshik"] = $_POST["id_postavshik"];
            $material["id_product"] = $id_product;
            #$material["last_input"] = date("Y-m-d H:i:s");
            if($id_product>0){
                Utils::insert(Material::tbl_name, $material);
            }
            
            header("Location:/shop/sklad/new");
            return true;
        }
        
         public function actionSpisat()
	{
            $list = Utils::getList(Material::tbl_name,null,"saldo>0");
            require_once ROOT.'/views/sklad/spisat.php';
            return true;
	}
        
        public function actionSpisatExec()
	{
            $update = array();
            $insert = array();
            if(isset($_POST)){
                foreach($_POST as $k=>$v){
                    $v = str_replace(",", ".", $v);
                    $v = floatval($v);
                    if($v>0){
                        $update[$k] = array("id" => $k,"saldo-" => $v);
                        $insert[$k] = array("id_material"=>$k,
                                        "id_user"=>$_SESSION["user"]["id"],
                                        "count" => $v,
                                        "datetime"=>date("Y-m-d H:i:s"));
                    }
                }
                if(count($update)>0){
                    Utils::update(Material::tbl_name, $update);
                    Utils::insert(Material::tbl_spisan, $insert);
                }
                unset($_POST);
            }
            header("Location:/shop/sklad/spisat");
            return true;
        }
        public function actionCleanPrihodList()
        {
            unset($_SESSION["prihodList"]);
            header("Location:/shop/sklad/new");
        }
}
?>