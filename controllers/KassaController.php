<?php
#User::checkAccess();
class KassaController
{
	public function actionIndex()
	{
            if(!isset($_SESSION["SaleID"])){
                $_SESSION["SaleID"] = 0;
            }
            $users = Utils::getList("users", "user_id,user_fio",null,null,"user_id");
            $firma = Utils::getList("postavshik", "postavshik_id id,postavshik_name name");
            require_once ROOT.'/views/kassa/index.php';
            return true;
	}
        public function actionGetByName($name){
            $name = substr($name,0,120);
            $res = Product::getByName($name);
            echo json_encode($res);
            return true;
        }
        public function actionGetByShCode($shcode){
            $shcode = substr($shcode,0,13);
            $res = Product::getByShCode($shcode); 
            echo json_encode($res);
            return true;
        }
        public function actionDelFromCart()
        {
            $arr = json_decode($_POST["arr"],1);
            $_SESSION["cart"] = $arr;
            $del["product_id"] = $arr["product_id"];
            $del["product_count"] = $arr["product_count"];
            $del["uid"] = $_SESSION["user"]["user_id"];
            $del["datetime"] = date("Y-m-d H:i:s");
            Utils::insert("log_delete",$del);
            echo 1;
            return true;
        }
        public function actionCloseSale()
        {
            $arr = json_decode($_POST["cart"],1);
            $sale_info = array();
            $sale = array();
            $sale["prodaja_sum_orig"] = 0;
            $sale["prodaja_sum_sell"] = 0;
            $sale["prodaja_foida"] = 0;
            
            foreach($arr as $k=>$v){
                if(!is_null($v)){
                    $item = array();
                    $item["product_id"] = $v["product_id"];
                    $item["prodaja_info_product_count"] = $v["product_count"];
                    $item["prodaja_info_sum_orig"] = $v["product_price_orig"]*$v["product_count"];
                    $item["prodaja_info_sum_sell"] = $v["product_price_sell"]*$v["product_count"];
                    $item["prodaja_info_foida"] = $item["prodaja_info_sum_sell"] - $item["prodaja_info_sum_orig"];
                    $sale_info[] = $item;
                    
                    $sale["prodaja_sum_orig"] += $item["prodaja_info_sum_orig"];
                    $sale["prodaja_sum_sell"] += $item["prodaja_info_sum_sell"];
                    $sale["prodaja_foida"] += $item["prodaja_info_foida"];
                }
            }
            $sale["prodaja_date"] = date("Y-m-d H:i:s");
            $sale["user_id"] = $_SESSION["user"]["user_id"];
            $sale["worker"] = $_POST["worker"];
            if($sale["prodaja_sum_orig"]>0){
                $sid = Utils::insert("prodaja", $sale);
                
                foreach($sale_info as $k=>$v){
                   $sale_info[$k]["prodaja_id"] = $sid;
                }
               Utils::insert("prodaja_info", $sale_info);
            }
            
           $list = Sale::getSaleInfoList($sid);
            if(intval($_POST["printSale"])>0){
                
                $data = array("list"=>$list,
                             "kassir"=> $_SESSION["user"]["user_fio"],
                             "worker"=>$_POST["worker"]);
                Utils::printCash($data, floatval(str_replace(",",".",$_POST["paySum"])),$comiss,date("Y-m-d H:i:s"));
            }
            $allSum = floatval(str_replace(",",".",$_POST["paySum"]));
            foreach($list as $ar){
                $allSum -= round($ar['price_out']*$ar['count'],2);
            }

            if($allSum < 0 && intval($_POST["worker"]) > 0){
                $pay = array("uid" => $_SESSION["user"]["user_id"],
                          "worker" => intval($_POST["worker"]),
                        "datetime" => date("Y-m-d H:i:s"),
                         "sum_out" => abs($allSum));
                Utils::insert("payments", $pay);
                $sal = array("user_id" => intval($_POST["worker"]),
                             "saldo+" => $allSum);
                Utils::update("users", $sal, "user_id");
            }
            header("Location:/shop/kassa/");
            return true;
        }
        
        public function actionShowSaleInfo($id)
        {
            $_SESSION["SaleID"] = $id;
            $info = Zakaz::getZakazList(null,array(Zakaz::state_podan),$id);
            echo json_encode($info);
            return true;
        }
        
        public function actionDelSaleInfo($id)
        {
            unset($_SESSION["SaleID"]);
            return true;
        }
        
        public function actionPrintCash($id)
        {
            $info = Zakaz::getSaleList($id);
            $info = $info[$id];
            $list = Zakaz::getZakazList(null,$status = array(Zakaz::state_podan),$id,1,1);
            $k = array_keys($list);
			$k = $k[0];
			$list[$k]["oficiant"] = $info["oficiant"];
			
			$comiss = 10;
            if(strlen($info["client"]) > 0){
                $comiss = 0;
            }
            if(substr($info["stol"],0,2) == 'С'){
                $comiss = 7;
            }
            Zakaz::printCash($list, 0,$comiss,date("Y-m-d H:i:s"));
            return true;
        }
               
        public function actionAddRashod()
        {
            $_POST["sum_out"] = floatval(str_replace(",", ".", $_POST["sum_out"]));
            if($_POST["sum_out"] < 1){
                header("Location:/shop/kassa");
            }
            $_POST["datetime"] = date("Y-m-d H:i:s");
            $_POST["uid"] = $_SESSION["user"]["user_id"];
            $data = array();
            $arr = array();
            if(isset($_POST["rec_type"])){
                unset($_POST["worker"]);
                unset($_POST["rec_type"]);
                $tbl_name = Firma::tbl_name;
                $key = "postavshik_id";
                $data["postavshik_id"] = $_POST["firma"];
                $data["postavshik_out+"] = $_POST["sum_out"];
                $data["postavshik_saldo-"] = $_POST["sum_out"];
                $firma = Utils::getList(Firma::tbl_name, "postavshik_name name", "postavshik_id=".intval($_POST["firma"]),1);
                $arr["firma"] = $firma["name"];
            }else{
                unset($_POST["firma"]);
                $tbl_name = User::tbl_name;
                $key = "user_id";
                $data["user_id"] = $_POST["worker"];
                $data["saldo-"] = $_POST["sum_out"];
                $worker = Utils::getList(User::tbl_name, "user_fio name", "user_id=".intval($_POST["worker"]),1);
                $arr["worker"] = $worker["name"];
            }
            
            $pid = Utils::insert(Payments::tbl_name, $_POST);
            Utils::update($tbl_name, $data,$key);
            
            $arr["kassir"] = $_SESSION["user"]["user_fio"];
            $arr["summa"] = $_POST["sum_out"];
            $arr["id"] = $pid;
            $arr["type"] = 'РАСХОД';
            Payments::printCash($arr);
            header("Location:/shop/kassa");
        }
}
?>