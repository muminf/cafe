<?php
class ZakazController
{
	public function actionIndex()
	{
            if($_SESSION["user"]["type"] == User::povar){
                header("Location:/shop/zakaz/kitchen");
            }
            $sSoboyList = Zakaz::getSsoboyList();
            $list = Zakaz::getStolList();
            require_once ROOT.'/views/zakaz/index.php';
            return true;
	}
        
        public function actionStol($id)
	{
            if($_SESSION["user"]["type"] == User::povar){
                header("Location:/shop/zakaz/kitchen");
            }
            $info = Zakaz::getStolList(null,$id);
            $menu = Product::getMenuList(1);
            $list = Zakaz::getZakazList($id,array(Zakaz::state_podan),0,1,1);
            $activeList = Zakaz::getZakazList($id,array(Zakaz::state_new,Zakaz::state_ready),0,1);
            $list = array_merge($list,$activeList);
            $busyList = Zakaz::getBusyStoli($id);
            $busyList["oficiant"] = (array_key_exists("oficiant",$busyList) ? $busyList["oficiant"] : "");
            if(isset($busyList["id_oficiant"])){
                if($_SESSION["user"]["type"] == User::oficiant && $_SESSION["user"]["id"] <> $busyList["id_oficiant"]){
                    header("Location:/shop/zakaz/");
                }
            }
            $prihodSum = Prihod::getItemSum($list);
            require_once ROOT.'/views/zakaz/stol.php';
            return true;
        }
        
        public function actionStol_tbl($id)
	{
            if($_SESSION["user"]["type"] == User::povar){
                header("Location:/shop/zakaz/kitchen");
            }
            $info = Zakaz::getStolList(null,$id);
            $menu = Product::getMenuList();
            $list = Zakaz::getZakazList($id,array(Zakaz::state_podan),0,1,1);
            $activeList = Zakaz::getZakazList($id,array(Zakaz::state_new,Zakaz::state_ready),0,1);
            $list = array_merge($list,$activeList);
            $busyList = Zakaz::getBusyStoli($id);
            $busyList["oficiant"] = (array_key_exists("oficiant",$busyList) ? $busyList["oficiant"] : "");
            if(isset($busyList["id_oficiant"])){
                if($_SESSION["user"]["type"] == User::oficiant && $_SESSION["user"]["id"] <> $busyList["id_oficiant"]){
                    header("Location:/shop/zakaz/");
                }
            }
            $prihodSum = Prihod::getItemSum($list);
            require_once ROOT.'/views/zakaz/stol_tbl.php';
            return true;
        }
        public function actionSsoboy($id)
	{
            if($_SESSION["user"]["type"] == User::povar){
                header("Location:/shop/zakaz/kitchen");
            }
            
            $info = Zakaz::getSsoboyList($id);
            if(!isset($info["client"])){
                header("Location:/shop/zakaz");
            }
            
            $menu = Product::getMenuList(1);
            $list = Zakaz::getZakazList(null,array(Zakaz::state_podan),$id,1,1);
            $activeList = Zakaz::getZakazList(null,array(Zakaz::state_new,Zakaz::state_ready),$id,1);
            $list = array_merge($list,$activeList);
            if($_SESSION["user"]["type"] == User::oficiant && $_SESSION["user"]["id"] <> $info["id_oficiant"]){
                header("Location:/shop/zakaz/");
            }
            #$prihodSum = Prihod::getItemSum($activeList);
            $prihodSum = Prihod::getItemSum($list);
            require_once ROOT.'/views/zakaz/ssoboy.php';
            return true;
        }
        
        public function actionSsoboy_tbl($id)
	{
            if($_SESSION["user"]["type"] == User::povar){
                header("Location:/shop/zakaz/kitchen");
            }
            
            $info = Zakaz::getSsoboyList($id);
            if(!isset($info["client"])){
                header("Location:/shop/zakaz");
            }
            
            $menu = Product::getMenuList();
            $list = Zakaz::getZakazList(null,array(Zakaz::state_podan),$id,1,1);
            $activeList = Zakaz::getZakazList(null,array(Zakaz::state_new,Zakaz::state_ready),$id,1);
            $list = array_merge($list,$activeList);
            if($_SESSION["user"]["type"] == User::oficiant && $_SESSION["user"]["id"] <> $info["id_oficiant"]){
                header("Location:/shop/zakaz/");
            }
            #$prihodSum = Prihod::getItemSum($activeList);
            $prihodSum = Prihod::getItemSum($list);
            require_once ROOT.'/views/zakaz/ssoboy_tbl.php';
            return true;
        }
        
        public function actionKitchen()
	{
            User::checkAccess();
            $list = Zakaz::getZakazList(null,array(Zakaz::state_new),0,0,0,Factory::kitchen);
            if(!isset($_SESSION["zakazCount"])){
                $_SESSION["zakazCount"] = 0;
            }
            require_once ROOT.'/views/zakaz/kitchen.php';
            User::checkAccess();
            return true;
	}
        
        public function actionBar()
	{
            #User::checkAccess();
            $list = Zakaz::getZakazList(null,array(Zakaz::state_new),0,0,0,Factory::bar);
            if(!isset($_SESSION["zakazCountBar"])){
                $_SESSION["zakazCountBar"] = 0;
            }
            User::checkAccess();
            require_once ROOT.'/views/zakaz/bar.php';
            
            return true;
	}
        
        
        
        public function actionAddToZakazListAjax($stol,$id,$count=0)
        {
            $menu = Product::getMenuList();
            $item = Utils::getList(Product::tbl_name, "id,saldo", "id=".intval($id));
            $saldo = $item[$id]["saldo"];
            $count = 1;
            #$count = ($count<1 ? 1 : $count);
            #if($saldo>0){
               # $count = ($count>$saldo ? $saldo : $count);
                Utils::update(Product::tbl_name, array("id"=>$id,"saldo-"=>$count));
                if(array_key_exists($stol,$_SESSION["zakazList"])){  
                    if(array_key_exists($id,$_SESSION["zakazList"][$stol])){ 
                        if($count>1){
                            $_SESSION["zakazList"][$stol][$id]["count"] = $count;
                        }else{
                            $_SESSION["zakazList"][$stol][$id]["count"]++;
                        }
                    }else{ 
                        $_SESSION["zakazList"][$stol][$id]["id"] = $id;
                        $_SESSION["zakazList"][$stol][$id]["id_sale"] = 0;
                        $_SESSION["zakazList"][$stol][$id]["count"] = $count;
                        $_SESSION["zakazList"][$stol][$id]["canceled_count"] = 0;
                       # $_SESSION["zakazList"][$stol][$id]["ready"] = 0;
                        $_SESSION["zakazList"][$stol][$id]["name"] = $menu[$id]["name"];
                        $_SESSION["zakazList"][$stol][$id]["price_in"] = $menu[$id]["price_in"];
                        $_SESSION["zakazList"][$stol][$id]["price_out"] = $menu[$id]["price_out"];
                        $_SESSION["zakazList"][$stol][$id]["name"] = $menu[$id]["name"];
                        $_SESSION["zakazList"][$stol][$id]["factory"] = $menu[$id]["id_factory"];
                        echo json_encode($_SESSION["zakazList"][$stol][$id]);
                    }
                }else{ 
                    $_SESSION["zakazList"][$stol] = array();
                    $_SESSION["zakazList"][$stol][$id]["id"] = $id;
                    $_SESSION["zakazList"][$stol][$id]["id_sale"] = 0;
                    $_SESSION["zakazList"][$stol][$id]["count"] = $count;
                    $_SESSION["zakazList"][$stol][$id]["canceled_count"] = 0;
                    #$_SESSION["zakazList"][$stol][$id]["ready"] = 0;
                    $_SESSION["zakazList"][$stol][$id]["name"] = $menu[$id]["name"];
                    $_SESSION["zakazList"][$stol][$id]["price_in"] = $menu[$id]["price_in"];
                    $_SESSION["zakazList"][$stol][$id]["price_out"] = $menu[$id]["price_out"];
                    $_SESSION["zakazList"][$stol][$id]["factory"] = $menu[$id]["id_factory"];
                    echo json_encode($_SESSION["zakazList"][$stol][$id]);
                }
           /* }else{
                echo 0;
            }*/
            #header("Location:/shop/zakaz/stol/".$stol);
            return true;
        }
        
        
        
        public function actionAddToZakazListAjaxSsoboy($stol,$id,$count=0)
        {
            $menu = Product::getMenuList();
            $item = Utils::getList(Product::tbl_name, "id,saldo", "id=".intval($id));
            $saldo = $item[$id]["saldo"];
            $count = 1;
            #$count = ($count<1 ? 1 : $count);
            #if($saldo>0){
              #  $count = ($count>$saldo ? $saldo : $count);
                Utils::update(Product::tbl_name, array("id"=>$id,"saldo-"=>$count));
                if(array_key_exists($stol,$_SESSION["zakazListSsoboy"])){  
                    if(array_key_exists($id,$_SESSION["zakazListSsoboy"][$stol])){ 
                        if($count>1){
                            $_SESSION["zakazListSsoboy"][$stol][$id]["count"] = $count;
                        }else{
                            $_SESSION["zakazListSsoboy"][$stol][$id]["count"]++;
                        }
                    }else{ 
                        $_SESSION["zakazListSsoboy"][$stol][$id]["id"] = $id;
                        $_SESSION["zakazListSsoboy"][$stol][$id]["id_sale"] = 0;
                        $_SESSION["zakazListSsoboy"][$stol][$id]["count"] = $count;
                        $_SESSION["zakazListSsoboy"][$stol][$id]["canceled_count"] = 0;
                       # $_SESSION["zakazListSsoboy"][$stol][$id]["ready"] = 0;
                        $_SESSION["zakazListSsoboy"][$stol][$id]["name"] = $menu[$id]["name"];
                        $_SESSION["zakazListSsoboy"][$stol][$id]["price_in"] = $menu[$id]["price_in"];
                        $_SESSION["zakazListSsoboy"][$stol][$id]["price_out"] = $menu[$id]["price_out"];
                        $_SESSION["zakazListSsoboy"][$stol][$id]["name"] = $menu[$id]["name"];
                        $_SESSION["zakazListSsoboy"][$stol][$id]["factory"] = $menu[$id]["id_factory"];
                        echo json_encode($_SESSION["zakazListSsoboy"][$stol][$id]);
                    }
                }else{ 
                    $_SESSION["zakazListSsoboy"][$stol] = array();
                    $_SESSION["zakazListSsoboy"][$stol][$id]["id"] = $id;
                    $_SESSION["zakazListSsoboy"][$stol][$id]["id_sale"] = 0;
                    $_SESSION["zakazListSsoboy"][$stol][$id]["count"] = $count;
                    $_SESSION["zakazListSsoboy"][$stol][$id]["canceled_count"] = 0;
                    #$_SESSION["zakazListSsoboy"][$stol][$id]["ready"] = 0;
                    $_SESSION["zakazListSsoboy"][$stol][$id]["name"] = $menu[$id]["name"];
                    $_SESSION["zakazListSsoboy"][$stol][$id]["price_in"] = $menu[$id]["price_in"];
                    $_SESSION["zakazListSsoboy"][$stol][$id]["price_out"] = $menu[$id]["price_out"];
                    $_SESSION["zakazListSsoboy"][$stol][$id]["factory"] = $menu[$id]["id_factory"];
                    echo json_encode($_SESSION["zakazListSsoboy"][$stol][$id]);
                }
          /*  }else{
                echo 0;
            }*/
            #header("Location:/shop/zakaz/stol/".$stol);
            return true;
        }
        
        
        public function actionAddToZakazList($stol,$id,$count=0)
        {
            $menu = Product::getMenuList();
            $item = Utils::getList(Product::tbl_name, "id,saldo", "id=".intval($id));
            $saldo = $item[$id]["saldo"];
            $count = 1;
            $count = ($count<1 ? 1 : $count);
           # if($saldo>0){
                $count = ($count>$saldo ? $saldo : $count);
                Utils::update(Product::tbl_name, array("id"=>$id,"saldo-"=>$count));
                if(array_key_exists($stol,$_SESSION["zakazList"])){  
                    if(array_key_exists($id,$_SESSION["zakazList"][$stol])){ 
                        if($count>1){
                            $_SESSION["zakazList"][$stol][$id]["count"] = $count;
                        }else{
                            $_SESSION["zakazList"][$stol][$id]["count"]++;
                        }
                    }else{ 
                        $_SESSION["zakazList"][$stol][$id]["id"] = $id;
                        $_SESSION["zakazList"][$stol][$id]["id_sale"] = 0;
                        $_SESSION["zakazList"][$stol][$id]["count"] = $count;
                       # $_SESSION["zakazList"][$stol][$id]["ready"] = 0;
                        $_SESSION["zakazList"][$stol][$id]["name"] = $menu[$id]["name"];
                        $_SESSION["zakazList"][$stol][$id]["price_in"] = $menu[$id]["price_in"];
                        $_SESSION["zakazList"][$stol][$id]["price_out"] = $menu[$id]["price_out"];
                        $_SESSION["zakazList"][$stol][$id]["name"] = $menu[$id]["name"];
                        $_SESSION["zakazList"][$stol][$id]["factory"] = $menu[$id]["id_factory"];
                        echo json_encode($_SESSION["zakazList"][$stol][$id]);
                    }
                }else{ 
                    $_SESSION["zakazList"][$stol] = array();
                    $_SESSION["zakazList"][$stol][$id]["id"] = $id;
                    $_SESSION["zakazList"][$stol][$id]["id_sale"] = 0;
                    $_SESSION["zakazList"][$stol][$id]["count"] = $count;
                    #$_SESSION["zakazList"][$stol][$id]["ready"] = 0;
                    $_SESSION["zakazList"][$stol][$id]["name"] = $menu[$id]["name"];
                    $_SESSION["zakazList"][$stol][$id]["factory"] = $menu[$id]["id_factory"];
                    $_SESSION["zakazList"][$stol][$id]["price_in"] = $menu[$id]["price_in"];
                    $_SESSION["zakazList"][$stol][$id]["price_out"] = $menu[$id]["price_out"];
                    echo json_encode($_SESSION["zakazList"][$stol][$id]);
                }
           # }
            header("Location:/shop/zakaz/stol/".$stol);
            return true;
        }
        
        public function actionMinusFromZakazList($stol,$id)
        {
            $_SESSION["zakazList"][$stol][$id]["count"]--;
            Utils::update(Product::tbl_name, array("id"=>$id,"saldo+"=>1));
            if($_SESSION["zakazList"][$stol][$id]["count"]<1){
                unset($_SESSION["zakazList"][$stol][$id]);
            }
            header("Location:/shop/zakaz/stol/".$stol);
            return true;
        }
        
        public function actionMinusFromZakazListSsoboy($stol,$id)
        {
            $_SESSION["zakazListSsoboy"][$stol][$id]["count"]--;
            Utils::update(Product::tbl_name, array("id"=>$id,"saldo+"=>1));
            if($_SESSION["zakazListSsoboy"][$stol][$id]["count"]<1){
                unset($_SESSION["zakazListSsoboy"][$stol][$id]);
            }
            header("Location:/shop/zakaz/stol/".$stol);
            return true;
        }
        
        public function actionAddToReadyList($id)
        {
            $arr = array("id"=>$id,"ready+"=>1);
            Utils::update(Sale::tbl_list, $arr);
            return true;
        }
        
        public function actionMinusFromReadyList($id)
        {
            $arr = array("id"=>$id,"ready-"=>1);
            Utils::update(Sale::tbl_list, $arr);
            return true;
        }
        
      /*  public function actionSendFromZakazList($stol,$id)
        { 
            if($_SESSION["zakazList"][$stol][$id]["id_sale"]<1){
                $_SESSION["zakazList"][$stol][$id]["id_sale"] = Zakaz::addNewZakaz($stol);
            }
            $item = $_SESSION["zakazList"][$stol][$id];
            $new = array();
            $new["id_product"] = $id;
            $new["price_in"] = $item["price_in"];
            $new["price_out"] = $item["price_out"];
            $new["count"] = $item["count"];
            $new["income"] = $item["price_out"]-$item["price_in"];
            $new["status"] = Zakaz::state_new;
            $new["id_sale"] = $_SESSION["zakazList"][$stol][$id]["id_sale"];
            Utils::insert(Sale::tbl_list, $new);
            unset($_SESSION["zakazList"][$stol][$id]);
            header("Location:/shop/zakaz/stol/".$stol);
            return true;
        }*/
        public function actionSendZakazList($stol)
        {
            $id = key($_SESSION["zakazList"][$stol]);

            $sale_id = Zakaz::checkSaleIsset($stol);
            if($sale_id < 1){
                $_SESSION["zakazList"][$stol][$id]["id_sale"] = Zakaz::addNewZakaz($stol);
            }else{
                $_SESSION["zakazList"][$stol][$id]["id_sale"] = $sale_id;
            }
            $list = array();
            $printList = array();
            $printListBar = array();
            foreach ($_SESSION["zakazList"][$stol] as $k=>$v){
                $item = $v;
                if($v["factory"] == Factory::kitchen){
                    $printList[] = array("name" => $item["name"], "count" => $item["count"]); 
                }
                if($v["factory"] == Factory::bar){
                    $printListBar[] = array("name" => $item["name"], "count" => $item["count"]); 
                }
                $new = array();
                $new["id_product"] = $k;
                $new["price_in"] = $item["price_in"];
                $new["price_out"] = $item["price_out"];
                $new["count"] = $item["count"];
                $new["income"] = $item["price_out"]-$item["price_in"];
                $new["status"] = Zakaz::state_podan;
                $new["id_sale"] = $_SESSION["zakazList"][$stol][$id]["id_sale"];
                $list[] = $new;
            }
            Utils::insert(Sale::tbl_list, $list);
            unset($_SESSION["zakazList"][$stol]);
			$info = array();
			$info = Zakaz::getStolList(null,$stol);
			$info["oficiant"] = $_SESSION["user"]["name"];
			$info["stol"] = $info["name"];
			#Utils::pre($info);exit;
			if(count($printList)>0){
				Zakaz::printKitchenCash($info,$printList);
				Zakaz::cutPaper();
			}
			if(count($printListBar)>0){
				Zakaz::printBarCash($info,$printListBar);
			}
            header("Location:/shop/zakaz/stol/".$stol);
            return true;
        }
        
        
        public function actionSendZakazListSsoboy($stol)
        {
            $id = key($_SESSION["zakazListSsoboy"][$stol]);

            $sale_id = $stol;
            $_SESSION["zakazListSsoboy"][$stol][$id]["id_sale"] = $sale_id;
            $list = array();
            $printList = array();
			$printListBar = array();
            foreach ($_SESSION["zakazListSsoboy"][$stol] as $k=>$v){
                $item = $v;
                if($v["factory"] == Factory::kitchen){
                    $printList[] = array("name" => $item["name"], "count" => $item["count"]); 
                }
				if($v["factory"] == Factory::bar){
                    $printListBar[] = array("name" => $item["name"], "count" => $item["count"]); 
                }
                $new = array();
                $new["id_product"] = $k;
                $new["price_in"] = $item["price_in"];
                $new["price_out"] = $item["price_out"];
                $new["count"] = $item["count"];
                $new["income"] = $item["price_out"]-$item["price_in"];
                $new["status"] = Zakaz::state_podan;
                $new["id_sale"] = $_SESSION["zakazListSsoboy"][$stol][$id]["id_sale"];
                $list[] = $new;
            }
            Utils::insert(Sale::tbl_list, $list);
            
            unset($_SESSION["zakazListSsoboy"][$stol]);
			$info["oficiant"] = $_SESSION["user"]["name"];
			$info["stol"] = 'С собой';
			if(count($printList)>0){
				Zakaz::printKitchenCash($info,$printList);
				Zakaz::cutPaper();
			}
			if(count($printListBar)>0){
				Zakaz::printBarCash($info,$printListBar);
			}
            header("Location:/shop/zakaz/Ssoboy/".$stol);
            return true;
        }
        
        public function actionSendFromKitchen($id)
        {
            $data = array("id" => $id, "status"=>Zakaz::state_podan);
            Utils::update(Sale::tbl_list, $data);
            $_SESSION["zakazCount"]--;
            header("Location:/shop/zakaz/kitchen");
            User::checkAccess();
            return true;
        }
        
        public function actionSendFromBar($id)
        {
            $data = array("id" => $id, "status"=>Zakaz::state_podan);
            Utils::update(Sale::tbl_list, $data);
            $_SESSION["zakazCountBar"]--;
            header("Location:/shop/zakaz/bar");
            User::checkAccess();
            return true;
        }
        
        public function actionServeToClient($id_stol,$id)
        {
            $list = array("id" => $id, "status"=>Zakaz::state_podan);
            
            $info = Utils::getList(Sale::tbl_list,null,"id=".$id);
            $info = $info[$id];
            $data = array("id" => $info["id_sale"],
                          "sum_in+" => $info["price_in"]*$info["count"],
                          "sum_out+" => $info["price_out"]*$info["count"]);
            
            Utils::update(Sale::tbl_list, $list);
            
            Utils::update(Sale::tbl_name, $data);
            
            header("Location:/shop/zakaz/stol/".$id_stol);
            return true;
        }
        
        public function actionCancelZakaz($id,$id_product,$id_stol)
        { 
            $count = intval($_POST["count"]);
            if(isset($_POST["remove"])){
                $insert = array();
                $insert[] = array("id_product"=>$id_product,
                                        "id_user"=>$_SESSION["user"]["id"],
                                        "count" => $count,
                                        "datetime"=>date("Y-m-d H:i:s"));
                if(count($insert)>0){
                    Utils::insert(Product::tbl_spisan, $insert);
                }
            }else{
                Utils::update(Product::tbl_name, array("saldo+"=>$count,"id"=>$id_product));
            }
            Utils::update(Sale::tbl_list, array("count-"=>$count,"canceled_count+"=>$count,"id"=>$id));
            header("Location:/shop/zakaz/stol/".$id_stol);
            return true;
        }
        
        public function actionAddNewSsoboy()
        {
            $new = array();
            $new["client"] = substr($_POST["client"],0,40);
            $new["beg_time"] = date("Y-m-d H:i:s");
            $new["id_oficiant"] = $_SESSION["user"]["id"];
            $id = Utils::insert(Sale::tbl_name, $new);
            header("Location:/shop/zakaz/Ssoboy/$id");
        }
}
?>