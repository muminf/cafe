<?php
#User::checkAccess();
class ReportController
{
        
	public function actionFilter()
	{
            if(isset($_POST["d1"])){
                $d1 = substr($_POST["d1"],0,10);
                $d2 = substr($_POST["d2"],0,10);
                $d1 = (strtotime($d1)> strtotime($d2) ? $d2 : $d1);
                $filter = "";
                foreach($_POST as $k=>$v){
                    $filter .= "/".$v;
                }
                header("Location:/shop/report".$filter);
            }
            return true;
        }
        
        public function actionPaymentsFilter()
        {
            if(isset($_POST["d1"])){
                $d1 = substr($_POST["d1"],0,10);
                $d2 = substr($_POST["d2"],0,10);
                $d1 = (strtotime($d1)> strtotime($d2) ? $d2 : $d1);
                header("Location:/shop/report/Payments/".$d1."/".$d2);
            }
            return true;
        }
        
        public function actionPayments($d1,$d2,$firma,$worker)
        {
            $usersArr = Utils::getList(User::tbl_name, "user_id,user_fio",null,null,"user_id");
            $firmaArr = Utils::getList(Firma::tbl_name, "postavshik_id id,postavshik_name name");
            $list = Payments::getList($d1,$d2,$firma,$worker);
            require_once ROOT.'/views/report/payments.php';
            return true;
        }
        
	public function actionCash($d1,$d2)
	{
            if(!isset($_SESSION["SaleID"])){
                $_SESSION["SaleID"] = 0;
            }
            
            $list = Zakaz::getSaleList(null,1,$d1,$d2);
            $id = $_SESSION["SaleID"];
            $info = ($id>0 ? Zakaz::getZakazList(null,null,$id,0,1) : array());
            
            $s = Sale::getSaleSumInPeriod($d1,$d2);
            $input = ($s["summa"]>0 ? round($s["summa"],2) : 0);
            $out = Utils::getList("docs","sum(sum_out) output","date(datetime)>='".Utils::dateToDbFormat($d1)."' AND date(datetime) <= '".Utils::dateToDbFormat($d2)."'",1);
            $output = ($out["output"]>0 ? round($out["output"],2) : 0);
            $saldo = $input-$output;
            require_once ROOT.'/views/report/cash.php';
            return true;
	}
        
        public function actionShowSaleInfo($id)
        {
            $_SESSION["SaleID"] = $id;
            $info = Zakaz::getZakazList(null,array(Zakaz::state_podan),$id);
            echo json_encode($info);
            return true;
        }
        
        public function actionPrintCash($id,$d1,$d2)
        {
            $info = Zakaz::getSaleList($id,1);
            $list = Zakaz::getZakazList(null,null,$id,0,1);
            $info = $info[$id];
            $comiss = ($info["sum_komiss"]>0 ? $info["sum_out"]/$info["sum_komiss"] : 0);
            #Utils::pre($list);
            if(Zakaz::printCash($list, $info["pay_sum"],$comiss,$info["end_time"])){
                header("Location:/shop/report/Cash/$d1/$d2");
            }
            return true;
        }
}
?>