<?php

class Spr1
{
    const spr_tbl = "spr";
    const spr_fld = "spr_fields";
    const spr_val = "spr_val";
    const ed_izm = 1;
    public static function getCategoryList()
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM ".self::spr_tbl;
        $qwr = $db->query($sql);
        $result = array();
        while($row = $qwr->fetch(PDO::FETCH_ASSOC)){
            $result[$row["id"]] = $row;
        }
        return $result;
    }
    
    public static function addCategory($data)
    {
        Utils::insert(self::spr_tbl, $data);
    }
    
    public static function addFields($id_category,$data)
    {
        $arr = array();
        foreach($data as $k=>$v){
            $arr[] = array("id_spr" => $id_category,"field"=>$v);
        }
        Utils::insert(self::spr_fld, $arr);
    }
    
    public static function addRow($data)
    {
        Utils::pre($data);
       # Utils::insert(self::spr_val, $data);
    }
    
    public static function getByCategory($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT v.id,f.field,v.value,v.id_collection
                FROM spr_val v 
                LEFT JOIN spr_fields f ON f.id = v.id_fld
                WHERE v.id_fld IN(SELECT id FROM spr_fields WHERE id_spr=".$id.")";
        $res = array();
        $q = $db->query($sql);
        while ($r = $q->fetch(PDO::FETCH_ASSOC)){
            $res[$r["id_collection"]]["id"] = intval($r["id_collection"]);
            $res[$r["id_collection"]][$r["field"]] = $r["value"];
        }
        return $res;
    }
}
?>