<?php

class Zakaz {
    
    const tbl_name = 'sale';
    const tbl_list = 'sale_list';
    const stolik = 8;
    const kabina = 9;
    const state_new = 10;
    const state_ready = 11;
    const state_podan = 12;
    const state_otmena = 13;
    
    public static function getStolList($id_type = null,$id=null){
        $db = Db::getConnection();
        $sql = "SELECT t.name,s.*,'' oficiant
                FROM stoliki s
                LEFT JOIN ".Spr::spr_val." t ON s.id_type=t.id
                WHERE 1=1 ";
        $sql .= ($id_type>0 ? " AND s.id_type = ".intval($id_type) : "");
        $sql .= ($id>0 ? " AND s.id = ".intval($id) : "");
        $sql .= " GROUP BY s.id ORDER BY s.id";
        $res = $db->query($sql);
        $list = array();
        if($res){
            if($id>0){
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list = $r;
                }
            }else{
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list[$r["id_type"]][$r["id"]] = $r;
                }
            }
        }
        return $list;
    }
    
    public static function getSsoboyList($id=null){
        $db = Db::getConnection();
        $sql = "SELECT s.id,s.client,s.id_oficiant,o.name oficiant
                FROM sale s
                LEFT JOIN ".User::tbl_name." o ON s.id_oficiant = o.id 
                WHERE s.id_stol is null and s.end_time is null ";
        $sql .= ($id>0 ? " AND s.id = ".intval($id) : "");
        $res = $db->query($sql);
        $list = array();
        if($res){
            if($id>0){
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list = $r;
                }
            }else{
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list[$r["id"]] = $r;
                }
            }
        }
        return $list;
    }
    
    public static function getZakazList($id_stol = null,$status = array(),$id_sale = 0,$active = 0,$groupByProduct=0,$id_factory = 0)
    { 
        $db = Db::getConnection();
        $sql = "SELECT
                    l.id,
                    l.id_product,
                    l.price_in,
                    l.price_out,
                    SUM(l.count) count,
                    SUM(l.canceled_count) canceled_count,
                    l.id_sale,
                    l.status id_status,
                    o.name oficiant, 
                    st.name stol, 
                    p.name product,
                    state.name status 
                  FROM
                    ".Sale::tbl_list." l 
                    LEFT JOIN ".Sale::tbl_name." s 
                      ON s.id = l.id_sale 
                    LEFT JOIN ".Agent::tbl_name." o 
                      ON o.id = s.id_oficiant 
                    LEFT JOIN stoliki st 
                      ON st.id = s.id_stol
                    LEFT JOIN ".Spr::spr_val." state 
                      ON state.id = l.status
                    LEFT JOIN ".Product::tbl_name." p
                      ON p.id = l.id_product
                    WHERE count>0";
        
        $sql .= ($id_stol>0 ? " AND s.id_stol=".intval($id_stol) : "");
        $sql .= (is_array($status) && count($status)>0 ? " AND state.id in(".implode(",",$status).")" : "");
        $sql .= ($id_sale>0 ? " AND l.id_sale = ".intval($id_sale) : "");
        $sql .= ($active>0 ? " AND s.end_time is null" : "");
        $sql .= ($id_factory>0 ? " AND p.id_factory = ".intval($id_factory) : "");
        if($groupByProduct>0){
            $sql .= " GROUP BY l.id_product,s.id_stol,state.id";
        }else{
            $sql .= " GROUP BY l.id,s.id_stol,state.id";
        }
        $list = array();
        if($res = $db->query($sql)){
            if($groupByProduct>0){
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list[$r["id_product"]] = $r;
                }
            }else{
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list[$r["id"]] = $r;
                }
            }
        }
        return $list;
    }
    
    public static function getZakazListKassa($id_stol = null,$status = null,$id_sale = 0,$active = 0)
    { 
        $db = Db::getConnection();
        $sql = "SELECT
                    l.id,
                    l.id_product,
                    l.price_in,
                    l.price_out,
                    SUM(l.count) count,
                    l.id_sale,
                    l.status id_status,
                    o.name oficiant, 
                    st.name stol, 
                    p.name product,
                    state.name status 
                  FROM
                    ".Sale::tbl_list." l 
                    LEFT JOIN ".Sale::tbl_name." s 
                      ON s.id = l.id_sale 
                    LEFT JOIN ".Agent::tbl_name." o 
                      ON o.id = s.id_oficiant 
                    LEFT JOIN stoliki st 
                      ON st.id = s.id_stol
                    LEFT JOIN ".Spr::spr_val." state 
                      ON state.id = l.status
                    LEFT JOIN ".Product::tbl_name." p
                      ON p.id = l.id_product
                    WHERE 1=1";
        
        $sql .= ($id_stol>0 ? " AND s.id_stol=".intval($id_stol) : "");
        $sql .= (count($status)>0 ? " AND state.id in(".implode(",",$status).")" : "");
        $sql .= ($id_sale>0 ? " AND l.id_sale = ".intval($id_sale) : "");
        $sql .= ($active>0 ? " AND s.end_time is null" : "");
        $sql .= " GROUP BY l.id_product,s.id_stol,state.id";
        $list = array();
        if($res = $db->query($sql)){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
    
    public static function addNewZakaz($id_stol)
    {
        $new = array();
        $new["beg_time"] = date("Y-m-d H:i:s");
        $new["id_oficiant"] = $_SESSION["user"]["id"];
        $new["id_stol"] = $id_stol;
        return Utils::insert(Sale::tbl_name, $new);
    }
    public static function getSaleList($id = null,$close = 0,$d1 = null,$d2 = null)
    {
        $db = Db::getConnection();
        $sql = "SELECT l.id,l.beg_time,l.end_time,of.name oficiant,SUM(sl.price_out*sl.count) sum_out,SUM(sl.price_in*sl.count) sum_in,l.client,st.name stol, l.pay_sum,l.all_sum,l.sum_komiss "
                . " FROM ".Sale::tbl_name." l"
                . " LEFT JOIN ".Sale::tbl_list." sl ON sl.id_sale = l.id"
                . " LEFT JOIN ".User::tbl_name." of ON l.id_oficiant = of.id"
                . " LEFT JOIN stoliki st ON l.id_stol = st.id"
                . " WHERE 1=1 ";
        $sql .= ($id>0 ? " AND l.id = ".intval($id) : "");
        $sql .= ($close<1 ? " AND l.end_time is null " : "");
        if(!is_null($d1)){
            $sql .= " AND date(beg_time) >= '".Utils::dateToDbFormat($d1)."' AND date(beg_time) <= '".Utils::dateToDbFormat($d2)."'";
        }
        $sql .= " GROUP BY l.id";
        $list = array();
        if($res = $db->query($sql)){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
    
    public static function getBusyStoli($id = 0)
    {
        $db = Db::getConnection();
        $sql = "SELECT s.id_stol id,o.name oficiant,o.id id_oficiant FROM ".Sale::tbl_name." s "
                . "LEFT JOIN ".User::tbl_name." o ON o.id = s.id_oficiant "
                . "WHERE s.end_time is null";
        $sql .= ($id>0 ? " AND s.id_stol = ".intval($id) : "");
        $list = array();
        if($res = $db->query($sql)){
            if($id>0){
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list = $r;
                }
            }else
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list[$r["id"]] = $r;
                }
            }   
        return $list;
    }
    public static function checkSaleIsset($id_stol){
        $item = Utils::getList(Sale::tbl_name, "id", "id_stol = '".intval($id_stol)."' and end_time is null order by id desc limit 1");
        if(is_array($item)){
            return key($item);
        }
    }
    public static function printCash($arr,$input,$comiss,$date){
		$k = array_keys($arr);
		$k = $k[0];
		$f = fopen('cash.txt','w');
		$str = '          Кафе "Шоми Амирон"  '.PHP_EOL;
		$str .= 'Дата: '.$date.PHP_EOL;
		$str .= 'Стол: '.$arr[$k]["stol"].PHP_EOL;
		$str .= 'Официант: '.$arr[$k]["oficiant"].PHP_EOL;
		$str .= '------------------------------------------------'.PHP_EOL;
		$allSum = 0;
		 foreach($arr as $ar){
			 $curSum = round($ar['price_out']*$ar['count'],2);
			 $allSum += $curSum;
			 $str .= $ar['product'].PHP_EOL;
			 $str .= '      '.$ar['price_out'].' x '.$ar['count'].' = '.$curSum.PHP_EOL;
		 }
         $str .= '------------------------------------------------'.PHP_EOL;
         
        $sumComiss = $allSum*$comiss/100;
        $itog = $allSum+$sumComiss;
        $str .= 'Сумма заказа: '.$allSum.PHP_EOL;
        $str .= 'За обслуживание: '.($sumComiss).PHP_EOL;
        $str .= 'Итог: '.$itog.PHP_EOL;
        if( $input > 0){ 
            $str .= 'Принято: '.$input.PHP_EOL;
            $str .= 'Сдача: '.($input-$itog).PHP_EOL;
            $str .= '------------------------------------------------'.PHP_EOL;
        }
         
	 fwrite($f,$str);
         exec("start /min notepad /P cash.txt");
	 fclose($f);
        header('Location: /shop/kassa');
    }
    
    public static function printKitchenCash($info,$list){
        
		$str = 'Дата: '.date("Y-m-d H:i:s").PHP_EOL;
		$str .= 'Официант: '.$info["oficiant"].PHP_EOL;
		$str .= 'Стол: '.$info["stol"].PHP_EOL;
		#echo $str; exit;
        $str .= '-----------------------------------------------------'.PHP_EOL;
		foreach($list as $ar){
			$str .= $ar['name'].'  x '.$ar['count'].PHP_EOL;
		}
        $str .= '-----------------------------------------------------'.PHP_EOL;
        $str .= '                                                     ';
        $str .= '                                                     ';
        $str .= '                                                     ';
		$f = fopen('kitchen.txt','w');
		fwrite($f,$str);
        exec("start /min notepad /PT \"kitchen.txt\" \"XP-80\"");
        fclose($f);
        #header('Location: /shop/kassa');
        return true;
    }
    public static function printBarCash($info,$list){
        
		$str = 'Дата: '.date("Y-m-d H:i:s").PHP_EOL;
		$str .= 'Официант: '.$info["oficiant"].PHP_EOL;
		$str .= 'Стол: '.$info["stol"].PHP_EOL;
		#echo $str; exit;
        $str .= '------------------------------------------------'.PHP_EOL;
		foreach($list as $ar){
			$str .= $ar['name'].'  x '.$ar['count'].PHP_EOL;
		}
        $str .= '------------------------------------------------'.PHP_EOL;
        $str .= '                                                     ';
        $str .= '                                                     ';
        $str .= '                                                     ';
		$f = fopen('bar.txt','w');
		fwrite($f,$str);
        exec("start /min notepad /P bar.txt");
        fclose($f);
        #header('Location: /shop/kassa');
        return true;
    }
    public static function cutPaper()
    {
         $url = "http://192.168.1.33/prt_test.htm?content=Welcome+to+use+the+impact+and+thermal+printer+manufactured+by+professional+POS+receipt+printer+company%21&Cutter=Cutter+Pape";
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);//для возврата результата в виде строки, вместо прямого вывода в браузер
         $returned = curl_exec($ch);
         curl_close ($ch);
         return true;
    }
     public static function printCashTable($arr,$input){
	 #$f = fopen('cash.txt','w');
	 $str = '<table id="cashTable" style="font-size:0.7em;padding:0;margin:0">';
	 $allSum = 0;
	 foreach($arr as $ar){
		 $curSum = round($ar['price_out']*$ar['count'],2);
		 $allSum += $curSum;
		 $str .= "<tr><td>".$ar['product'].'</td><td>'.$ar['price_out'].'</td><td> x </td><td>'.$ar['count'].'</td><td> = </td><td>'.$curSum."</td></tr>";
	 }
         
	 $str .= '<tr><td colspan="3">Итог:</td><td colspan="3">'.$allSum."</td></tr>";
         $str .= '<tr><td colspan="3">Принято:</td><td colspan="3">'.$input."</td></tr>";
         $str .= '<tr><td colspan="3">Сдача:</td><td colspan="3">'.($input-$allSum)."</td></tr>";
         $str .= "</table>";
	 return $str;	 
    }
}

