<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author M_Fayziev
 */
class Material {
    const tbl_name = "material";
    const tbl_spisan = "material_spisan";
    public static function getByShCode($shcode)
    {
        $db = Db::getConnection();
        $sql = 'SELECT m.*,a.name postavshik,e.name ed_izm_name FROM '.self::tbl_name.' m '
               . 'LEFT JOIN agents a on a.id = m.id_postavshik '
               . 'LEFT JOIN '.Spr::spr_val.' e on e.id = m.ed_izm'
               . ' WHERE shcode = :shcode'
               . ' AND a.id_type=1 LIMIT 1';
        $result = $db->prepare($sql);
        $result->bindParam(':shcode', $shcode, PDO::PARAM_STR);
        $result->execute();         
        $material = $result->fetch(PDO::FETCH_ASSOC);
        if($material){
            return $material;
        }
        return false;
    }
    
    public static function getByName($name)
    {
        $db = Db::getConnection();
        $sql = "SELECT m.*,a.name postavshik,e.name ed_izm_name FROM ".self::tbl_name." m "
        . "LEFT JOIN agents a on a.id = m.id_postavshik "
        . "LEFT JOIN ".Spr::spr_val." e on e.id = m.ed_izm "
        . "WHERE m.name like CONCAT(:name, '%') "
        . "AND a.id_type=".Agent::postavshik;
        $result = $db->prepare($sql);
        $name = urldecode($name);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->execute();
        $list = array();
        if($result){
            while($r = $result->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
    
    public static function getById($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT m.*,a.name postavshik,e.name ed_izm_name FROM ".self::tbl_name." m "
        . "LEFT JOIN agents a on a.id = m.id_postavshik "
        . "LEFT JOIN ".Spr::spr_val." e on e.id = m.ed_izm "
        . "WHERE m.id = :id "
        . "AND a.id_type=".Agent::postavshik." LIMIT 1";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $list = array();
        if($result){
            $list = $result->fetch(PDO::FETCH_ASSOC);
        }
        return $list;
    }
    
    public static function addNewMaterial($data)
    {
        Utils::insert(self::tbl_name, $data);
    }
    
    public static function updateMaterial($data)
    {
        
    }
    public static function getSaldo($id=null)
    {
        $db = Db::getConnection();
        $sql = "SELECT m.id,.m.ed_izm,m.`id_category`,m.`name`,m.`shcode`,m.`id_postavshik`,m.`price`,m.`id_product`,e.name ed_izm_name,p.name postavshik,IF(m.`id_product`>0,pr.`saldo`,m.`saldo`) saldo,m.last_input "
                . " FROM ".Material::tbl_name." m "
                . "LEFT JOIN ".Spr::spr_val." e ON m.ed_izm = e.id "
                . "LEFT JOIN ".Agent::tbl_name." p on m.id_postavshik = p.id "
                . "LEFT JOIN ".Product::tbl_name." pr ON pr.id=m.id_product";
        
        if($id>0){
            $sql .= " WHERE m.id = ".intval($id);
        }
        $res = $db->query($sql);
        $list = array();
        if($res){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
}
