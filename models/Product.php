<?php

class Product
{
    const tbl_name = "products";
    const tbl_sostav = "product_sostav";
    const tbl_ready = "product_ready";
    const tbl_spisan = "product_spisan";
    
    public static function getSostav($id,$id_sostav = null)
    { 
        $db = Db::getConnection();
        $sql = "SELECT s.*,m.name,m.price,e.name ed_izm_name,round(s.count*m.price,3) summa FROM ".Product::tbl_sostav." s "
                . "LEFT JOIN ".Material::tbl_name." m ON s.id_material = m.id "
                . "LEFT JOIN ".Spr::spr_val." e ON m.ed_izm = e.id "
                . "WHERE s.id_product = ".intval($id);
        $sql .= (intval($id_sostav)>0 ? " AND s.id = ".intval($id_sostav)." LIMIT 1" : "");
        $list = array();
        $res = $db->query($sql);
        if($res){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
    
    public static function addToSostav($arr)
    { 
        $new = array();
        $new["id_product"] = $arr["id_product"];
        $new["id_material"] = $arr["id"];
        $new["count"] = $arr["count"];
        return Utils::insert(Product::tbl_sostav, $new);
    }
    public static function delFromSostav($id)
    {
        return Utils::Delete(Product::tbl_sostav,array($id));
    }

    public static function getList($id=null)
    {
        $db = Db::getConnection();
        $sql = "SELECT p.*,c.name category,f.name factory,s.name status_text
                FROM ".Product::tbl_name." p
                LEFT JOIN ".Spr::spr_val." c ON p.id_category=c.id
                LEFT JOIN ".Spr::spr_val." f ON p.id_factory=f.id
                LEFT JOIN ".Spr::spr_val." s ON p.status=s.id";
        $list = array();
        if($res = $db->query($sql)){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
    public static function getByName($name)
    {
        $db = Db::getConnection();
        $sql = "SELECT p.product_id,p.product_name,p.product_shtrixkod,p.product_price_sell,p.product_price_orig,m.magazin_product_count product_saldo,e.chenak FROM ".Product::tbl_name." p "
                . "LEFT JOIN magazin m ON m.product_id=p.product_id "
                . "LEFT JOIN chenak e on e.chenak_id = p.chenak_id "
                . "WHERE p.product_name like CONCAT('%',:name, '%') ";
        $result = $db->prepare($sql);
        $name = urldecode($name);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->execute();
        $list = array();
        if($result){
            while($r = $result->fetch(PDO::FETCH_ASSOC)){
                $list[] = $r;
            }
        }
        return $list;
    }
    
    public static function getByShCode($shcode)
    {
        $db = Db::getConnection();
        $sql = 'SELECT p.product_id,p.product_name,p.product_shtrixkod,p.product_price_sell,p.product_price_orig,e.chenak,m.magazin_product_count product_saldo FROM '.Product::tbl_name.' p '
               . "LEFT JOIN magazin m ON m.product_id=p.product_id "
               . 'LEFT JOIN chenak e on e.chenak_id = p.chenak_id'
               . ' WHERE p.product_shtrixkod = :shcode LIMIT 1';
        $result = $db->prepare($sql);
        $result->bindParam(':shcode', $shcode, PDO::PARAM_STR);
        $result->execute();         
        $product = $result->fetch(PDO::FETCH_ASSOC);
        if($product){
            return $product;
        }
        return false;
    }
    
    public static function UpdateOrigPrice($id_product)
    {
        $db = Db::getConnection();
        $sql = "UPDATE product p
                SET p.price_in = (SELECT ROUND(SUM(s.count*m.price),3) 
                    FROM product_sostav s 
                    LEFT JOIN material m ON s.id_material = m.id
                    WHERE s.id_product=".intval($id_product)."), income = round(p.price_out-p.price_in,2)
                WHERE p.id = ".intval($id_product);
        $db->query($sql);
    }
    
    public static function getMenuList($by_cat = false)
    {
        $db = Db::getConnection();
        $sql = "SELECT p.*,cat.name category FROM ".Product::tbl_name." p
                LEFT JOIN ".Spr::spr_val." cat ON p.id_category=cat.id"
                . " WHERE p.id_category is not null ";
                 #" AND p.saldo>0";
        $list = array();
        if($res = $db->query($sql)){
            if($by_cat){
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list[$r["id_category"]]["category"] = $r["category"];
                    $list[$r["id_category"]]["items"][$r["id"]] = $r;
                }
            }else{
                while($r = $res->fetch(PDO::FETCH_ASSOC)){
                    $list[$r["id"]] = $r;
                }
            }
        }
        return $list;
    }
}
?>