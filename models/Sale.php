<?php

class Sale {
    
    const tbl_name = 'prodaja';
    const tbl_list = 'prodaja_info';
    const status_new = 10;
    const status_ready = 11;
    
    public static function getSaleSumInPeriod($date,$date2=null)
    {
        $db = Db::getConnection();
        $date2 = (is_null($date2) ? $date : $date2);
        $sql = "SELECT SUM(s.prodaja_sum_sell) summa FROM ".Sale::tbl_name." s WHERE 1=1";
        if(!is_null($date2)){
            $sql .= " and DATE(s.prodaja_date) >= '".Utils::dateToDbFormat($date)."' and DATE(s.prodaja_date) <='".Utils::dateToDbFormat($date2)."'";
        }
        $res = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        return $res["summa"];
    }
    
    public static function getSaleInfoList($sid)
    {
        if(intval($sid)<1){
            return array();
        }
        $sql = "SELECT l.prodaja_info_product_count count,
                        p.product_name product,
                       (l.prodaja_info_sum_sell/l.prodaja_info_product_count) price_out
                 FROM prodaja_info l
                 LEFT JOIN products p ON p.product_id = l.product_id
                 WHERE l.prodaja_id=".intval($sid);
        $db = Db::getConnection();
        $res = array();
        if($q = $db->query($sql)){
            while($r = $q->fetch(PDO::FETCH_ASSOC)){
                $res[] = $r;
            }
        }
        return $res;
    }
}

