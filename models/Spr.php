<?php

class Spr
{
    const spr_tbl = "spr";
    const spr_val = "spr_val";
    const ed_izm = 1;
    const menu_category = 2;
    const menu_factory = 3;
    const seat_type = 4; #тип посадочного места (столик/кабина)
    const zakaz_status = 5;
    const komiss_prs = 6;
    const user_type = 7;
    const status = 8;
    
    const status_active = 20;
    const status_inactive = 21;
    
    public static function getCategoryList()
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM ".self::spr_tbl;
        $qwr = $db->query($sql);
        $result = array();
        while($row = $qwr->fetch(PDO::FETCH_ASSOC)){
            $result[$row["id"]] = $row;
        }
        return $result;
    }
    
    public static function addCategory($data)
    {
        Utils::insert(self::spr_tbl, $data);
    }
    
    public static function addRow($data)
    {
        Utils::insert(self::spr_val, $data);
    }
    
    public static function getByCategory($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT v.id,v.name
                FROM spr s
                LEFT JOIN spr_val v ON v.id_spr = s.id
                WHERE s.id = ".intval($id);
        $res = array();
        if($q = $db->query($sql)){
            while ($r = $q->fetch(PDO::FETCH_ASSOC)){
                $res[$r["id"]]["name"] = $r["name"];
            }
        }
        return $res;
    }
}
?>