<?php

class Utils
{
    const shcode_prefix = "822";
    public static function pre($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
  /*  public static function isAssoc($arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }*/
  /*  public static function isAssoc($arr)
    {
        IF(IS_ARRAY($arr) && !IS_NUMERIC(ARRAY_SHIFT(ARRAY_KEYS($arr)))){
            RETURN TRUE;
        }
        RETURN FALSE;
    }*/
    public static function isAssoc($array){
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }
    public static function insert($tbl_name,$data)
    {
        $db = Db::getConnection();
        if(!self::isAssoc($data)){
            $keys = array_keys($data);
            $firstKey = $keys[0];
            $row_length = count($data[$firstKey]);
            $nb_rows = count($data);
            $length = $nb_rows * $row_length;

            /* Fill in chunks with '?' and separate them by group of $row_length */
            $args = implode(',', array_map(
                function($el) { return '('.implode(',', $el).')'; },
                array_chunk(array_fill(0, $length, '?'), $row_length)
            ));

            $params = array();
            foreach($data as $row)
            {
               foreach($row as $value)
               {
                  $params[] = $value;
               }
            }
            $columns = implode(",",array_keys($data[$firstKey]));
            $sql = "INSERT INTO ".$tbl_name." (".$columns.") VALUES ".$args;
        }else{
            $columns = implode(",",array_keys($data));
            $params = array_values($data);
            $sql = "INSERT INTO ".$tbl_name." (".$columns.") VALUES(".substr(str_repeat("?,", count($data)),0,-1).")";
        }
        
        $stmt = $db->prepare($sql);
        $stmt->execute($params);
        #$stmt->debugDumpParams();

        return $db->lastInsertId();
    }
    
    public static function getOptionList($data,$id = 0,$key = "name")
    {
        $str = "";
        if(Utils::isAssoc($data)){
            $list[] = $data;
        }else{
            $list = $data;
        }
        foreach ($list as $k=>$v){
            $sel = ($id == $k ? "selected" : "");
            $str .= "<option value='".$k."' ".$sel.">".$v[$key]."</option>";
        }
        return $str;
    }
    public static function getList($tbl_name,$fields = null,$where = null,$single = 0,$key = "id")
    {
       $db = Db::getConnection();
       $fields = (!is_null($fields) ? $fields : "*");
       $sql = 'SELECT '.$fields.' FROM '.$tbl_name;
       $sql .= (!is_null($where) ? " WHERE ".$where : "");
       $result = $db->query($sql);
       $list = array();
       if($result){
            if($single>0){
               $list = $result->fetch(PDO::FETCH_ASSOC);
            }else{
                while($r = $result->fetch(PDO::FETCH_ASSOC)){
                    $list[$r[$key]] = $r;
                }
           }
            if(is_array($list)){
                return $list;
            }
       }
       #$result->debugDumpParams();
      
       return false;
    }
    public static function genShcode(){
        return self::shcode_prefix.substr(time(),1,10);
    }
    public static function getLastId($tbl_name)
    {
        $db = Db::getConnection();
        $sql = "SELECT id FROM ".$tbl_name." ORDER by id DESC LIMIT 1";
        $res = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        return $res["id"];
    }
    public static function update($tbl_name,$data,$key = "id",$where="")
    {
        $db = Db::getConnection();
        $list = $data;
        $res;
        if(self::isAssoc($data)){
            $list = array();
            $list[] = $data;
        }
        foreach($list as $k=>$v){
            $sql = "UPDATE ".$tbl_name." SET ";
            $id = 0;
            foreach($v as $kk=>$vv){
                $ink = substr($kk,-1,1);
                if($ink == "+" || $ink == "-"){
                    $kk = substr($kk,0,-1);
                    $sql .= $kk." = ".$kk.$ink." '".floatval($vv)."',";
                }else{
                    $sql .= $kk." = '".$vv."',";
                }
            }
            $sql = substr($sql,0,-1);
            $sql .= " WHERE 1=1 and ".$key." = ".$v[$key]." ".$where;
            $res = $db->query($sql);
        }
        return $res->rowCount();
    }
    public static function dateToDbFormat($date)
    {
        return date("Y-m-d",strtotime($date));
    }
    public static function Delete($tbl_name,$ids=array(),$where=null)
    {
        $db = Db::getConnection();
        $sql = "DELETE FROM ".$tbl_name." WHERE 1=1 ";
        if(count($ids)>0){
            $sql .= " AND id in(".implode(",",$ids).")";
        }
        if(!is_null($where)){
            $sql .= " ".$where;
        }echo $sql;
        return $db->query($sql);
    }
    public static function getFloatVal($str){
        return floatval(str_replace(",", ".", $str));
    }
    public static function printCash($arr,$input,$comiss,$date){
        $f = fopen('cash.txt','w');
        $str = '          Магазин "Навруз"  '.PHP_EOL;
        $str .= 'Дата: '.$date.PHP_EOL;
        $str .= 'Кассир: '.$arr["kassir"].PHP_EOL;
        $str .= '------------------------------------------------'.PHP_EOL;
        $allSum = 0;
        foreach($arr["list"] as $ar){
                 $curSum = round($ar['price_out']*$ar['count'],2);
                 $allSum += $curSum;
                 $str .= $ar['product'].PHP_EOL;
                 $str .= '      '.$ar['price_out'].' x '.$ar['count'].' = '.$curSum.PHP_EOL;
        }
        $str .= '------------------------------------------------'.PHP_EOL;
        
        $str .= 'Итог: '.$allSum.PHP_EOL;
        $sd = ($input-$allSum < 0 ? 'Долг' : 'Сдача');
        $workerStr = "";
        if($arr["worker"]>0){
            $worker = Utils::getList("users", "user_fio", "user_id=".intval($arr["worker"]),1,"user_fio");
            $workerStr = 'Сотрудник: '.$worker["user_fio"].PHP_EOL;
        }
        
        if( $input > 0){ 
            $str .= 'Принято: '.$input.PHP_EOL;
            $str .= $sd.': '.abs($input-$allSum).PHP_EOL;
            $str .= $workerStr;
            $str .= '------------------------------------------------'.PHP_EOL;
        }
         
	fwrite($f,$str);
       // exec("start /min notepad /P cash.txt");
	fclose($f);
        header('Location: /shop/kassa');
    }
}
?>