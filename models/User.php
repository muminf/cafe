<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author M_Fayziev
 */
class User {
    
    const tbl_name = 'users';
    const admin = 15;
    const kassir = 16;
    const sklad = 17;
    
    public static function register($name,$login,$password)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO user(name,login,password) '
                .' VALUES(:name,:login,:password)';
       $result = $db->prepare($sql);
       $password = md5($password);
       $result->bindParam(':name', $name, PDO::PARAM_STR);
       $result->bindParam(':login', $login, PDO::PARAM_STR);
       $result->bindParam(':password', $password, PDO::PARAM_STR);
       return $result->execute();         
    }
    public static function checkName($name){
        if(strlen($name)>=2){
            return true;
        }
        return false;
    }
    public static function checkLogin($login){
        if(strlen($login)>=2){
            return true;
        }
        return false;
    }
    public static function checkPassword($password){
        if(strlen($password)>=3){
            return true;
        }
        return false;
    }
    public static function checkEmail($email){
        /*if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;*/return true;
    }
    public static function checkLoginExists($login){
        $db = Db::getConnection();
        $sql = 'SELECT count(*) FROM '.User::tbl_name.' WHERE user_login=:login';
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->execute();
        if($result->fetchColumn())
            return false;
        return true;
    }
    public static function checkUserData($login,$password)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM '.User::tbl_name.' WHERE user_login = :login AND user_password = :password';
        #$password = self::getPasswordHash($password);
        $result = $db->prepare($sql);
        $result->bindParam(':login',$login,PDO::PARAM_STR);
        $result->bindParam(':password',$password,PDO::PARAM_STR);
        $result->execute();
        $user = $result->fetch();
        if($user){
            return $user['user_id'];
        }
        return false;
    }   
    public static function auth($userId)
    {
        $_SESSION['user'] = self::getUserById($userId);
    }
    
    public static function checkLogged()
    {
        if(isset($_SESSION['user'])){
            return $_SESSION['user'];
        }
        header("Location: /user/login");
    }
    public static function isGuest()
    {
        if(!isset($_SESSION['user'])){
            return true;
        }
        return false;
    }
    public static function getUserById($id)
    {
        if($id){
            $db = Db::getConnection();
            $sql = 'SELECT * FROM '.User::tbl_name.' WHERE user_id=:id and user_status='.intval(Spr::status_active);
            
            $result = $db->prepare($sql);
            $result->bindParam(':id',$id,PDO::PARAM_INT);
            
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            
            return $result->fetch(PDO::FETCH_ASSOC);
        }
    }
    
    
    public static function getUserInfoById($id)
    {
        if($id){
            $db = Db::getConnection();
            $sql = 'SELECT * FROM '.User::tbl_name.' WHERE user_id=:id and user_status='.intval(Spr::status_active);
            
            $result = $db->prepare($sql);
            $id = intval($id);
            $result->bindParam(':id',$id,PDO::PARAM_INT);
            
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            
            return $result->fetch(PDO::FETCH_ASSOC);
        }
    }
    public static function edit($userId,$name,$password)
    {
        $db = Db::getConnection();
        $sql = 'UPDATE user SET name = :name, password = :password '
                .' WHERE id = :id';
       $result = $db->prepare($sql);
       $result->bindParam(':id', $userId, PDO::PARAM_INT);
       $result->bindParam(':name', $name, PDO::PARAM_STR);
       $result->bindParam(':password', $password, PDO::PARAM_STR);
       return $result->execute();         
    }
    public static function checkAccess()
    {
        $loc = "";
        switch($_SESSION['user']["user_type"]){
            case User::kassir:
                $loc = '/shop/kassa';
                if($_SERVER["REQUEST_URI"] <> $loc){
                    header('Location:'.$loc);
                }
            break;
            case User::sklad:
                $loc = '/shop/sklad/';
                if($_SERVER["REQUEST_URI"] <> $loc){
                    header('Location:'.$loc);
                }
            break;
        }
    }
    
    public static function getPasswordHash($login){
        return md5($login);
    }
}
