<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payments
 *
 * @author M_Fayziev
 */
class Payments {
    
    const tbl_name = "payments";
    
    public static function getList($d1,$d2,$firma=0,$worker=0)
    {
        $db = Db::getConnection();
        $sql = "SELECT p.*,u.user_fio worker_name,f.postavshik_name firma_name 
                FROM ".Payments::tbl_name." p
                LEFT JOIN ".User::tbl_name." u ON u.user_id = p.worker
                LEFT JOIN ".Firma::tbl_name." f ON f.postavshik_id = p.firma "
                . "WHERE date(p.datetime)>='".Utils::dateToDbFormat($d1)."' and date(p.datetime) <= '".Utils::dateToDbFormat($d2)."'";
        if($firma>0){
            $sql .= " and p.firma = ".$firma;
        }
        if($worker>0){
            $sql .= " and p.worker = ".$worker;
        }
        $list = array();
        if($res = $db->query($sql)){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
    
    public static function printCash($arr){
        $f = fopen('pay.txt','w');
        $str = '      Магазин "Навруз"  '.PHP_EOL;
        $str .= '------------------------------------------------'.PHP_EOL;
        $str .= '           '.$arr["type"].'  '.PHP_EOL;
        $str .= '------------------------------------------------'.PHP_EOL;
        $str .= 'Дата: '.date("d.m.Y H:i:s").PHP_EOL;
        $str .= 'Кассир: '.$arr["kassir"].PHP_EOL;
        $str .= 'Сумма: '.$arr["summa"].'с.'.PHP_EOL;
        if(strlen($arr["firma"])>0){
            $str .= 'Фирма: '.$arr["firma"].PHP_EOL;
        }
        if(strlen($arr["worker"])>0){
            $str .= 'Сотрудник: '.$arr["worker"].PHP_EOL;
        }
        $str .= 'Номер плат: '.$arr["id"].PHP_EOL;
        $str .= '------------------------------------------------'.PHP_EOL;
        
         
	fwrite($f,$str);
       // exec("start /min notepad /P pay.txt");
	fclose($f);
        header('Location: /shop/kassa');
    }
}
