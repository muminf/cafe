<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author M_Fayziev
 */
class Prihod {
    const tbl_name = "prihod";
    const tbl_list = "prihod_list";
   
    public static function add($data)
    {
       $prihod["summa"] = self::getItemSum($data);
       $prihod["id_user"] = $_SESSION["user"]["id"];
       $prihod["datetime"] = date("Y-m-d H:i:s");
       $id = Utils::insert(self::tbl_name, $prihod);
       foreach ($data as $k=>$v){
           $data["id_prihod"] = $id;
       }
       self::addItemList($data);
    }
        
    public static function getItemList($data)
    {
        
    }
    
    public static function getList($d1,$d2)
    {
        #$list = Utils::getList(Prihod::tbl_name,null," date(datetime)>='".Utils::dateToDbFormat($d1)."' and date(datetime) <= '".Utils::dateToDbFormat($d2)."'");
        $db = Db::getConnection();
        $sql = "SELECT p.*,u.name user "
                . "FROM ".Prihod::tbl_name." p "
                . "LEFT JOIN ".Agent::tbl_name." u ON p.id_user = u.id "
                . "WHERE date(datetime)>='".Utils::dateToDbFormat($d1)."' and date(datetime) <= '".Utils::dateToDbFormat($d2)."'";
        $list = array();
        if($res = $db->query($sql)){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
    
    public static function getItemSum($data)
    {
        $summa = 0;
        if(Utils::isAssoc($data)){
            $summa = $data["price_out"]*$data["count"];
        }else{
            foreach ($data as $k=>$v){
                $summa += $v["price_out"]*$v["count"];
            }
        }
        return $summa;
    }
    
    public static function getItemSumPrihod($data)
    {
        $summa = 0;
        if(Utils::isAssoc($data)){
            $summa = $data["price"]*$data["count"];
        }else{
            foreach ($data as $k=>$v){
                $summa += $v["price"]*$v["count"];
            }
        }
        return $summa;
    }
    
    public static function getItemListByIds($ids)
    {
        $db = Db::getConnection();
        $ids = implode(",", $ids);
        $sql = "SELECT l.*,m.shcode,m.name,p.name postavshik,e.name ed_izm_name "
                . "FROM ".Prihod::tbl_list." l "
                . "LEFT JOIN ".Material::tbl_name." m ON l.id_material = m.id "
                . "LEFT JOIN ".Agent::tbl_name." p ON l.id_postavshik = p.id "
                . "LEFT JOIN ".Spr::spr_val." e ON m.ed_izm = e.id "
                . " WHERE l.id_prihod IN(".$ids.")";
        $res = $db->query($sql);
        $list = array();
        if($res){
            while($r = $res->fetch(PDO::FETCH_ASSOC)){
                $list[$r["id"]] = $r;
            }
        }
        return $list;
    }
}
