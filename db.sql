/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.30-MariaDB : Database - afkor_mag
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`afkor_mag` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `afkor_mag`;

/*Table structure for table `accesses` */

DROP TABLE IF EXISTS `accesses`;

CREATE TABLE `accesses` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_url` varchar(150) DEFAULT NULL,
  `access_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `brak` */

DROP TABLE IF EXISTS `brak`;

CREATE TABLE `brak` (
  `brak_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `brak_otkuda` int(11) DEFAULT NULL COMMENT '0-sklad 1-vitrina',
  `brak_count` double DEFAULT NULL,
  `brak_comment` text,
  `brak_date` datetime DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`brak_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `chenak` */

DROP TABLE IF EXISTS `chenak`;

CREATE TABLE `chenak` (
  `chenak_id` int(11) NOT NULL AUTO_INCREMENT,
  `chenak` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`chenak_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `cl_in` */

DROP TABLE IF EXISTS `cl_in`;

CREATE TABLE `cl_in` (
  `cl_in_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `cl_in_sum_dod` double DEFAULT NULL,
  `cl_in_date` datetime DEFAULT NULL,
  PRIMARY KEY (`cl_in_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `cl_out` */

DROP TABLE IF EXISTS `cl_out`;

CREATE TABLE `cl_out` (
  `cl_out_id` int(11) NOT NULL AUTO_INCREMENT,
  `prodaja_id` int(11) DEFAULT NULL,
  `cl_out_sum_nadod` double DEFAULT NULL,
  `cl_out_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cl_out_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `clients` */

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(250) DEFAULT NULL,
  `client_tel` varchar(50) DEFAULT NULL,
  `client_number_car` varchar(50) DEFAULT NULL,
  `client_in` double DEFAULT '0',
  `client_out` double DEFAULT '0',
  `client_saldo` double DEFAULT '0',
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `delete_log` */

DROP TABLE IF EXISTS `delete_log`;

CREATE TABLE `delete_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `product_count` double DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `discount` */

DROP TABLE IF EXISTS `discount`;

CREATE TABLE `discount` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_unique_id` varchar(250) DEFAULT NULL,
  `discount_percent` double DEFAULT NULL,
  `discount_name` varchar(100) DEFAULT NULL,
  `discount_client_fio` varchar(250) DEFAULT NULL,
  `discount_reg_date` datetime DEFAULT NULL,
  PRIMARY KEY (`discount_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `kassa` */

DROP TABLE IF EXISTS `kassa`;

CREATE TABLE `kassa` (
  `kassa_id` int(11) NOT NULL AUTO_INCREMENT,
  `kassa_in` double DEFAULT NULL,
  `kassa_out` double DEFAULT NULL,
  `kassa_saldo` double DEFAULT NULL,
  `kassa_ostatok` double DEFAULT NULL,
  `kassa_date` date DEFAULT NULL,
  `kassa_status` int(11) DEFAULT '0',
  `kassa_number` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kassa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=997 DEFAULT CHARSET=utf8;

/*Table structure for table `kassa_info` */

DROP TABLE IF EXISTS `kassa_info`;

CREATE TABLE `kassa_info` (
  `kassa_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `kassa_id` int(11) DEFAULT NULL,
  `kassa_info_type` varchar(50) DEFAULT NULL,
  `kassa_info_type_id` int(11) DEFAULT NULL,
  `kassa_info_in` double DEFAULT NULL,
  `kassa_info_out` double DEFAULT NULL,
  `kassa_info_date` datetime DEFAULT NULL,
  `kassa_info_status` int(11) DEFAULT NULL,
  `kassa_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`kassa_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=472697 DEFAULT CHARSET=utf8;

/*Table structure for table `kurs` */

DROP TABLE IF EXISTS `kurs`;

CREATE TABLE `kurs` (
  `kurs_id` int(11) NOT NULL AUTO_INCREMENT,
  `kurs_usd` double DEFAULT NULL,
  `kurs_eur` double DEFAULT NULL,
  `kurs_rur` double DEFAULT NULL,
  PRIMARY KEY (`kurs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `kurs_info` */

DROP TABLE IF EXISTS `kurs_info`;

CREATE TABLE `kurs_info` (
  `kurs_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `kurs_info_usd` double DEFAULT NULL,
  `kurs_info_eur` double DEFAULT NULL,
  `kurs_info_rur` double DEFAULT NULL,
  `kurs_info_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kurs_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `log_delete` */

DROP TABLE IF EXISTS `log_delete`;

CREATE TABLE `log_delete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `product_count` double DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Table structure for table `magazin` */

DROP TABLE IF EXISTS `magazin`;

CREATE TABLE `magazin` (
  `magazin_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `magazin_product_count` double DEFAULT NULL,
  `magazin_last_prihod` datetime DEFAULT NULL,
  `magazin_last_prodaja` datetime DEFAULT NULL,
  PRIMARY KEY (`magazin_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20509 DEFAULT CHARSET=utf8;

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sum_in` double DEFAULT NULL,
  `sum_out` double DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `worker` int(11) DEFAULT NULL,
  `firma` int(11) DEFAULT NULL,
  `nazn` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Table structure for table `post_in` */

DROP TABLE IF EXISTS `post_in`;

CREATE TABLE `post_in` (
  `post_in_id` int(11) NOT NULL AUTO_INCREMENT,
  `postavshik_id` int(11) DEFAULT NULL,
  `prihod_id` int(11) DEFAULT NULL,
  `post_in_sum` double DEFAULT NULL,
  `post_in_date` datetime DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`post_in_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27768 DEFAULT CHARSET=utf8;

/*Table structure for table `post_out` */

DROP TABLE IF EXISTS `post_out`;

CREATE TABLE `post_out` (
  `post_out_id` int(11) NOT NULL AUTO_INCREMENT,
  `postavshik_id` int(11) DEFAULT NULL,
  `post_out_sum` double DEFAULT NULL,
  `post_out_date` datetime DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`post_out_id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

/*Table structure for table `postavshik` */

DROP TABLE IF EXISTS `postavshik`;

CREATE TABLE `postavshik` (
  `postavshik_id` int(11) NOT NULL AUTO_INCREMENT,
  `postavshik_name` varchar(100) DEFAULT NULL,
  `postavshik_in` double DEFAULT '0',
  `postavshik_out` double DEFAULT '0',
  `postavshik_saldo` double DEFAULT '0',
  PRIMARY KEY (`postavshik_id`)
) ENGINE=MyISAM AUTO_INCREMENT=522 DEFAULT CHARSET=utf8;

/*Table structure for table `prihod` */

DROP TABLE IF EXISTS `prihod`;

CREATE TABLE `prihod` (
  `prihod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prihod_date` datetime DEFAULT NULL,
  `prihod_date_postavki` date DEFAULT NULL,
  `prihod_sum` double DEFAULT NULL,
  `prihod_status` int(11) DEFAULT '0',
  `prihod_confirm_date` datetime DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`prihod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21764 DEFAULT CHARSET=utf8;

/*Table structure for table `prihod_info` */

DROP TABLE IF EXISTS `prihod_info`;

CREATE TABLE `prihod_info` (
  `prihod_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `prihod_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `postavshik_id` int(11) DEFAULT NULL,
  `prihod_info_product_count` double DEFAULT NULL,
  `prihod_info_product_price_orig` double DEFAULT NULL,
  `prihod_info_product_price_sell` double DEFAULT NULL,
  `prihod_info_product_price_foida` double DEFAULT NULL,
  `prihod_info_product_price_sum` double DEFAULT NULL,
  `prihod_info_confirm_count` double DEFAULT NULL,
  `prihod_info_date` datetime DEFAULT NULL,
  `prihod_info_confirm_date` datetime DEFAULT NULL,
  `prihod_info_status` int(11) DEFAULT '0',
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`prihod_info_id`),
  KEY `prihod_id` (`prihod_id`)
) ENGINE=MyISAM AUTO_INCREMENT=76075 DEFAULT CHARSET=utf8;

/*Table structure for table `prodaja` */

DROP TABLE IF EXISTS `prodaja`;

CREATE TABLE `prodaja` (
  `prodaja_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `prodaja_date` datetime DEFAULT NULL,
  `prodaja_sum_orig` double DEFAULT NULL,
  `prodaja_sum_sell` double DEFAULT NULL,
  `prodaja_status` int(11) DEFAULT '0' COMMENT '0-ok, 1-nim vozvrat, 2polnyi vozvrat',
  `prodaja_vozvrat_date` datetime DEFAULT NULL,
  `prodaja_foida` double DEFAULT NULL,
  `worker` int(11) DEFAULT NULL,
  `discount` double DEFAULT '0',
  PRIMARY KEY (`prodaja_id`)
) ENGINE=MyISAM AUTO_INCREMENT=495836 DEFAULT CHARSET=utf8;

/*Table structure for table `prodaja_info` */

DROP TABLE IF EXISTS `prodaja_info`;

CREATE TABLE `prodaja_info` (
  `prodaja_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `prodaja_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `prodaja_info_product_count` double DEFAULT NULL,
  `product_info_id` int(11) DEFAULT NULL,
  `prodaja_info_sum_orig` double DEFAULT NULL,
  `prodaja_info_sum_sell` double DEFAULT NULL,
  `prodaja_info_foida` double DEFAULT NULL,
  `prodaja_info_vozvrat` double DEFAULT '0',
  PRIMARY KEY (`prodaja_info_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1038105 DEFAULT CHARSET=utf8;

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_shtrixkod` varchar(50) DEFAULT NULL,
  `product_name` text,
  `chenak_id` int(11) DEFAULT NULL,
  `postavshik_id` int(11) DEFAULT NULL,
  `product_min` int(11) DEFAULT NULL,
  `product_price_orig` double DEFAULT NULL,
  `product_price_foida` double DEFAULT NULL,
  `product_price_sell` double DEFAULT NULL,
  `product_description` varchar(100) DEFAULT NULL,
  `product_info_id` int(11) DEFAULT NULL,
  `product_exp_date` date DEFAULT NULL,
  `plu` int(11) DEFAULT NULL,
  `plu2` int(11) DEFAULT NULL,
  `upd` int(1) DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18132 DEFAULT CHARSET=utf8;

/*Table structure for table `products_info` */

DROP TABLE IF EXISTS `products_info`;

CREATE TABLE `products_info` (
  `product_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_info_price_orig_old` double DEFAULT NULL,
  `product_info_price_sell_old` double DEFAULT NULL,
  `product_info_price_orig_new` double DEFAULT NULL,
  `product_info_price_sell_new` double DEFAULT NULL,
  `product_info_date_change` datetime DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35215 DEFAULT CHARSET=utf8;

/*Table structure for table `report` */

DROP TABLE IF EXISTS `report`;

CREATE TABLE `report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_title` varchar(150) DEFAULT NULL,
  `report_sql` text,
  `report_sklad` int(11) DEFAULT '0',
  PRIMARY KEY (`report_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Table structure for table `sklad` */

DROP TABLE IF EXISTS `sklad`;

CREATE TABLE `sklad` (
  `sklad_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `sklad_product_count` double DEFAULT NULL,
  `sklad_last_prihod` datetime DEFAULT NULL,
  `sklad_last_vyvoz` datetime DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`sklad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15465 DEFAULT CHARSET=utf8;

/*Table structure for table `sync` */

DROP TABLE IF EXISTS `sync`;

CREATE TABLE `sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sqls` text,
  `status` int(11) DEFAULT '0',
  `ins_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(50) DEFAULT NULL,
  `user_password` varchar(50) DEFAULT NULL,
  `user_fio` varchar(200) DEFAULT NULL,
  `user_access` varchar(50) DEFAULT NULL,
  `user_online` int(11) DEFAULT '0',
  `user_sing_in` datetime DEFAULT '2014-01-01 00:00:00',
  `user_sing_out` datetime DEFAULT '2014-01-01 00:00:00',
  `user_savdo` double DEFAULT '0',
  `user_status` int(11) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `saldo` double DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Table structure for table `vozvrat` */

DROP TABLE IF EXISTS `vozvrat`;

CREATE TABLE `vozvrat` (
  `vozvrat_id` int(11) NOT NULL AUTO_INCREMENT,
  `prodaja_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `vozvrat_count` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `vozvrat_date` datetime DEFAULT NULL,
  `vozvrat_sum` double DEFAULT NULL,
  `vozvrat_cost` double DEFAULT NULL,
  `vozvrat_foida` double DEFAULT NULL,
  PRIMARY KEY (`vozvrat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1716 DEFAULT CHARSET=utf8;

/*Table structure for table `vyvoz` */

DROP TABLE IF EXISTS `vyvoz`;

CREATE TABLE `vyvoz` (
  `vyvoz_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `vyvoz_product_count` double DEFAULT NULL,
  `vyvoz_date` datetime DEFAULT NULL,
  `user_set` int(11) DEFAULT NULL,
  `user_get` int(11) DEFAULT NULL,
  PRIMARY KEY (`vyvoz_id`)
) ENGINE=MyISAM AUTO_INCREMENT=81141 DEFAULT CHARSET=utf8;

/* Function  structure for function  `get_chenak_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_chenak_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_chenak_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT chenak INTO res FROM chenak WHERE chenak_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_client_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_client_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_client_name`(id INT(11)) RETURNS varchar(200) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(200);
	SELECT client_name INTO res FROM clients WHERE client_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_discount` */

/*!50003 DROP FUNCTION IF EXISTS `get_discount` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_discount`(id INT(11)) RETURNS double
BEGIN
	DECLARE res Double;
	SELECT discount_percent INTO res FROM discount WHERE discount_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_ID` */

/*!50003 DROP FUNCTION IF EXISTS `get_ID` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_ID`(wkod VARCHAR(50)) RETURNS int(11)
BEGIN
	DECLARE res INT(11);
	SELECT product_id INTO res FROM products WHERE product_shtrixkod=wkod;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_otsek_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_otsek_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_otsek_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT otsek_name INTO res FROM otsek WHERE otsek_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_postavshik_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_postavshik_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_postavshik_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT postavshik_name INTO res FROM postavshik WHERE postavshik_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_post_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_post_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_post_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT postavshik_name INTO res FROM postavshik WHERE postavshik_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_product_otsek_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_product_otsek_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_product_otsek_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT otsek_name INTO res FROM otsek WHERE otsek_id=(SELECT otsek_id FROM products WHERE product_id=id);
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_car_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_car_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_car_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT CONCAT(car_marka,' ',car_model) INTO res FROM cars WHERE car_id in (select car_id from products where product_id=id);
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_chenak_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_chenak_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_chenak_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT chenak INTO res FROM chenak WHERE chenak_id IN (SELECT chenak_id FROM products WHERE product_id=id);
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_count_mag` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_count_mag` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_count_mag`(id INT(11)) RETURNS double
BEGIN
	DECLARE res DOUBLE;
	SET res=0;
	SELECT magazin_product_count INTO res FROM magazin WHERE product_id=id;
	RETURN res;
END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_count_sk` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_count_sk` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_count_sk`(id INT(11)) RETURNS double
BEGIN
	DECLARE res DOUBLE;
	set res=0;
	SELECT sklad_product_count INTO res FROM sklad WHERE product_id=id;
	RETURN res;
END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_min` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_min` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_min`(id INT(11)) RETURNS int(11)
BEGIN
	DECLARE res int(11);
	set res=0;
	SELECT product_min INTO res FROM products WHERE product_id=id;
	RETURN res;
END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_name`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT product_name INTO res FROM products WHERE product_shtrixkod=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_name_id` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_name_id` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_name_id`(id INT(11)) RETURNS text CHARSET utf8
BEGIN
	DECLARE res text;
	SELECT product_name INTO res FROM products WHERE product_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_ostatok` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_ostatok` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_ostatok`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT magazin_product_count INTO res FROM magazin WHERE product_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_postuplen` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_postuplen` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_postuplen`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SET res=0;
	SELECT SUM(prihod_info_product_count) INTO res FROM prihod_info WHERE product_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_price` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_price` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_price`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SELECT product_price_orig INTO res FROM products WHERE product_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_pro_prodan` */

/*!50003 DROP FUNCTION IF EXISTS `get_pro_prodan` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_pro_prodan`(id INT(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(100);
	SET res=0;
	SELECT SUM(prodaja_info_product_count) INTO res FROM prodaja_info WHERE product_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_shtrixkod` */

/*!50003 DROP FUNCTION IF EXISTS `get_shtrixkod` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_shtrixkod`(id INT(11)) RETURNS varchar(50) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(50);
	SELECT product_shtrixkod INTO res FROM products WHERE product_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `get_user_name` */

/*!50003 DROP FUNCTION IF EXISTS `get_user_name` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_user_name`(id INT(11)) RETURNS varchar(200) CHARSET utf8
BEGIN
	DECLARE res VARCHAR(200);
	SELECT user_fio INTO res FROM users WHERE user_id=id;
	RETURN res;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `vozvrat_process` */

/*!50003 DROP PROCEDURE IF EXISTS  `vozvrat_process` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `vozvrat_process`(vozvrat_count DOUBLE,vozvrat_sum DOUBLE,vozvrat_product_id INT,vozvrat_prodaja_id INT,vozvrat_user_id INT,vozvrat_data DATETIME,product_price_orig DOUBLE)
BEGIN  
DECLARE p_s INT; 
UPDATE magazin SET magazin_product_count=magazin_product_count+vozvrat_count WHERE product_id=vozvrat_product_id;
UPDATE prodaja_info SET prodaja_info_vozvrat=prodaja_info_vozvrat+vozvrat_count WHERE  prodaja_id=vozvrat_prodaja_id AND product_id=vozvrat_product_id;
SELECT IF(SUM(prodaja_info_product_count)<=SUM(prodaja_info_vozvrat),2,1) AS prodaja_status INTO p_s FROM prodaja_info WHERE prodaja_id=vozvrat_prodaja_id;
UPDATE prodaja SET prodaja_vozvrat_date=vozvrat_data,prodaja_status=p_s WHERE prodaja_id=vozvrat_prodaja_id;
INSERT INTO vozvrat(prodaja_id,product_id,vozvrat_count,user_id,vozvrat_date,vozvrat_sum,vozvrat_cost,vozvrat_foida) VALUES (vozvrat_prodaja_id,vozvrat_product_id,vozvrat_count,vozvrat_user_id,vozvrat_data,vozvrat_sum,product_price_orig*vozvrat_count,ROUND((vozvrat_sum-(product_price_orig*vozvrat_count)),2));
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
